#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 09:45:15 2020.

@author: freya
"""

from shapely.geometry import Point, Polygon
import math
import matplotlib.pyplot as plt
import numpy as np

# import seaborn as sns
# from matplotlib.colors import LogNorm
from scipy import interpolate
from matplotlib.lines import Line2D

# def get_csv():
#     file="2013_2_14_nights.csv"#"201{}_{}_14_nights.csv".format(year, split)
#     csv = np.genfromtxt ('./data/new_data/sorted/write_to/{}'.format(file),
#                          delimiter=",",
#                          skip_header=1,dtype=float,
#                          usecols=(0,1,2)
#                           )
#     return csv
# csv_xy=get_csv()

"define the mirror"
r = 5.0000000
a = 2 * (r) / math.sqrt(3.0000000)
b = 1.0000000 / math.sqrt(3.0000000)
h = 0.50000000
# hexagon= Polygon([(0,a), (r,a/2), (r, -a/2), (0,-a), (-r, -a/2), (-r, a/2)])
a = 5.50000000
hexagon = Polygon(
    [
        (-0.5 * b, a),
        (0.5 * b, a),
        (b, a - h),
        (2 * b, a - h),
        (2.5 * b, a - 2 * h),
        (3.5 * b, a - 2 * h),
        (4 * b, a - 3 * h),
        (5 * b, a - 3 * h),
        (5.5 * b, a - 4 * h),
        (6.5 * b, a - 4 * h),
        (7 * b, a - 5 * h),
        (8 * b, a - 5 * h),
        (8.5 * b, a - 6 * h),  # corner
        (8 * b, a - 7 * h),
        (8.5 * b, a - 8 * h),
        (8 * b, a - 9 * h),
        (8.5 * b, a - 10 * h),
        (8 * b, a - 11 * h),
        (8.5 * b, a - 12 * h),
        (8 * b, a - 13 * h),
        (8.5 * b, a - 14 * h),
        (8 * b, a - 15 * h),
        (8.5 * b, a - 16 * h),
        (8 * b, a - 17 * h),  # corner
        (7 * b, a - 17 * h),
        (6.5 * b, a - 18 * h),
        (5.5 * b, a - 18 * h),
        (5 * b, a - 19 * h),
        (4 * b, a - 19 * h),
        (3.5 * b, a - 20 * h),
        (2.5 * b, a - 20 * h),
        (2 * b, a - 21 * h),
        (1 * b, a - 21 * h),
        (0.5 * b, a - 22 * h),
        (-0.5 * b, a - 22 * h),  # corner
        (-b, a - 21 * h),
        (-2 * b, a - 21 * h),
        (-2.5 * b, a - 20 * h),
        (-3.5 * b, a - 20 * h),
        (-4 * b, a - 19 * h),
        (-5 * b, a - 19 * h),
        (-5.5 * b, a - 18 * h),
        (-6.5 * b, a - 18 * h),
        (-7 * b, a - 17 * h),
        (-8 * b, a - 17 * h),
        (-8.5 * b, a - 16 * h),  # corner
        (-8 * b, a - 15 * h),
        (-8.5 * b, a - 14 * h),
        (-8 * b, a - 13 * h),
        (-8.5 * b, a - 12 * h),
        (-8 * b, a - 11 * h),
        (-8.5 * b, a - 10 * h),
        (-8 * b, a - 9 * h),
        (-8.5 * b, a - 8 * h),
        (-8 * b, a - 7 * h),
        (-8.5 * b, a - 6 * h),
        (-8 * b, a - 5 * h),  # corner
        (-7 * b, a - 5 * h),
        (-6.5 * b, a - 4 * h),
        (-5.5 * b, a - 4 * h),
        (-5 * b, a - 3 * h),
        (-4 * b, a - 3 * h),
        (-3.5 * b, a - 2 * h),
        (-2.5 * b, a - 2 * h),
        (-2 * b, a - 1 * h),
        (-1 * b, a - 1 * h),
    ]
)

# xh,yh=hexhex.exterior.xy
# x,y,=hexagon.exterior.xy
# fig,ax=plt.subplots(1,1)
# ax.set_aspect(1./ax.get_data_ratio())
# ax.plot(xh,yh)
# ax.plot(x,y)
# ax.plot([-6,0,6],[0,0,0])
# ax.plot([0,0,0],[-6,0,6])


"////"
covered = []
# def get_coverage_x_y(x_y):
#     coverage=[]
#     for row in x_y:
#         xp=(26/13) * row[0]
#         yp=(26/13)*row[1]
#         centre_annulus=Point(xp,yp)
#         "////"
#         outer_circle=centre_annulus.buffer(5.5)
#         inner_circle=centre_annulus.buffer(3.5/2)
#         annulus=outer_circle.difference(inner_circle)
#         mirror_covered=hexagon.intersection(annulus)
#         percentage_mirror=100*mirror_covered.area/hexagon.area
# #        print(percentage_mirror)
#         coverage.append(percentage_mirror)
#     return coverage

"get percentage of mirror covered by MT annulus centered at any given xy"


def get_coverage_MT(x, y):
    #    coverage=[]
    #    for i in range(len(x)):
    xp = (26 / 13) * x
    yp = (26 / 13) * y
    centre_annulus = Point(xp, yp)
    "////"
    outer_circle = centre_annulus.buffer(4.5, resolution=500)
    inner_circle = centre_annulus.buffer(1, resolution=500)
    annulus = outer_circle.difference(inner_circle)
    mirror_covered = hexagon.intersection(annulus)
    percentage_mirror = 100 * mirror_covered.area / hexagon.area
    #        print(percentage_mirror)
    #    coverage.append(percentage_mirror)
    return percentage_mirror


"get percentage of mirror covered by annulus centered at any given xy"


def get_coverage(x, y):
    #    coverage=[]
    #    for i in range(len(x)):
    xp = (26 / 13) * x
    yp = (26 / 13) * y
    centre_annulus = Point(xp, yp)
    "////"
    outer_circle = centre_annulus.buffer(5.5, resolution=500)
    inner_circle = centre_annulus.buffer(3.5 / 2, resolution=500)
    annulus = outer_circle.difference(inner_circle)
    mirror_covered = hexagon.intersection(annulus)
    percentage_mirror = 100 * mirror_covered.area / hexagon.area
    #        print(percentage_mirror)
    #    coverage.append(percentage_mirror)
    return percentage_mirror


# cmap = sns.cubehelix_palette(as_cmap=True) #colour scheme
# x_range=np.arange(0,2.5, 0.1)
# y_range=np.arange(0,2.5,0.1)
# x_y=[]
# for i in range(len(x_range)):
#     for k in range(len(y_range)):
#         x_y.append((x_range[i], y_range[k]))

# fig = plt.figure()
# ax = fig.add_subplot(1,1,1)

# fig, ax = plt.subplots()
# ax.set_aspect(1./ax.get_data_ratio())
# coverage=get_coverage_x_y(x_y)
# x_y_np=np.asarray(x_y)
# plot=ax.scatter(x_y_np[:,0], x_y_np[:,1], c=coverage, cmap=cmap)
# fig.colorbar(plot)


# at=0.5*a
# rt=0.5*r


# xlist = np.linspace(-3, 3, 50)
# ylist = np.linspace(-3, 3, 50)
# X, Y = np.meshgrid(xlist, ylist)
# Z = np.vectorize(get_coverage)(X,Y)
# fig,ax=plt.subplots(1,1)
# ax.set_aspect(1./ax.get_data_ratio())
# cp = ax.contour(X,Y, Z)
# ax.clabel(cp, inline=True, inline_spacing=3, rightside_up=True)#, colors='k', fontsize=8, fmt=fmt)
# ax.set_xlim(-3,3)
# ax.set_ylim(-3,3)
# plt.show()
# ax.set_title('Percentage of mirror covered by pupil for tracker x-y')
# ax.set_xlabel('x (m)')
# ax.set_ylabel('y (m)')
# ax.grid(True, linestyle="--", alpha=0.1, color='grey')
# plt.show()
"hext"


def hext(a, b, h):
    a = 0.5 * a
    b = 0.5 * b
    h = 0.5 * h
    hexagon = Polygon(
        [
            (-0.5 * b, a),
            (0.5 * b, a),
            (b, a - h),
            (2 * b, a - h),
            (2.5 * b, a - 2 * h),
            (3.5 * b, a - 2 * h),
            (4 * b, a - 3 * h),
            (5 * b, a - 3 * h),
            (5.5 * b, a - 4 * h),
            (6.5 * b, a - 4 * h),
            (7 * b, a - 5 * h),
            (8 * b, a - 5 * h),
            (8.5 * b, a - 6 * h),  # corner
            (8 * b, a - 7 * h),
            (8.5 * b, a - 8 * h),
            (8 * b, a - 9 * h),
            (8.5 * b, a - 10 * h),
            (8 * b, a - 11 * h),
            (8.5 * b, a - 12 * h),
            (8 * b, a - 13 * h),
            (8.5 * b, a - 14 * h),
            (8 * b, a - 15 * h),
            (8.5 * b, a - 16 * h),
            (8 * b, a - 17 * h),  # corner
            (7 * b, a - 17 * h),
            (6.5 * b, a - 18 * h),
            (5.5 * b, a - 18 * h),
            (5 * b, a - 19 * h),
            (4 * b, a - 19 * h),
            (3.5 * b, a - 20 * h),
            (2.5 * b, a - 20 * h),
            (2 * b, a - 21 * h),
            (1 * b, a - 21 * h),
            (0.5 * b, a - 22 * h),
            (-0.5 * b, a - 22 * h),  # corner
            (-b, a - 21 * h),
            (-2 * b, a - 21 * h),
            (-2.5 * b, a - 20 * h),
            (-3.5 * b, a - 20 * h),
            (-4 * b, a - 19 * h),
            (-5 * b, a - 19 * h),
            (-5.5 * b, a - 18 * h),
            (-6.5 * b, a - 18 * h),
            (-7 * b, a - 17 * h),
            (-8 * b, a - 17 * h),
            (-8.5 * b, a - 16 * h),  # corner
            (-8 * b, a - 15 * h),
            (-8.5 * b, a - 14 * h),
            (-8 * b, a - 13 * h),
            (-8.5 * b, a - 12 * h),
            (-8 * b, a - 11 * h),
            (-8.5 * b, a - 10 * h),
            (-8 * b, a - 9 * h),
            (-8.5 * b, a - 8 * h),
            (-8 * b, a - 7 * h),
            (-8.5 * b, a - 6 * h),
            (-8 * b, a - 5 * h),  # corner
            (-7 * b, a - 5 * h),
            (-6.5 * b, a - 4 * h),
            (-5.5 * b, a - 4 * h),
            (-5 * b, a - 3 * h),
            (-4 * b, a - 3 * h),
            (-3.5 * b, a - 2 * h),
            (-2.5 * b, a - 2 * h),
            (-2 * b, a - 1 * h),
            (-1 * b, a - 1 * h),
        ]
    )
    return hexagon


# hext=hext(a,b,h)
# xt,yt=hext.exterior.xy
# ax.plot(xt,yt)

# xlist = np.linspace(-3, 3, 50)
# ylist = np.linspace(-3, 3, 50)
# X, Y = np.meshgrid(xlist, ylist)
# Z = np.vectorize(get_coverage_MT)(X,Y)
# fig,ax=plt.subplots(1,1)
# ax.set_aspect(1./ax.get_data_ratio())
# cp = ax.contour(X,Y, Z)
# ax.clabel(cp, inline=True, inline_spacing=3, rightside_up=True)#, colors='k', fontsize=8, fmt=fmt)
# ax.set_xlim(-3,3)
# ax.set_ylim(-3,3)
# plt.show()
# ax.set_title('Percentage of mirror covered by pupil for MiniTracker x-y')
# ax.set_xlabel('x (m)')
# ax.set_ylabel('y (m)')
# ax.grid(True, linestyle="--", alpha=0.1, color='grey')
# plt.show()


"SLOW PART"
calculate = False
if calculate == True:
    x = np.arange(-5.6, 5.6, 0.05)
    y = np.arange(-5.6, 5.6, 0.05)
    xx, yy = np.meshgrid(x, y)
    z = np.vectorize(get_coverage)(xx, yy)
    z_MT = np.vectorize(get_coverage_MT)(xx, yy)
    # f = interpolate.interp2d(x, y, z, kind='cubic')
    f_MT = interpolate.interp2d(x, y, z_MT, kind='cubic')
    # x_low=np.arange(-3.2, 3.2, 0.15)
    # y_low=np.arange(-3.2, 3.2, 0.15)
    # xxlow,yylow=np.meshgrid(x_low,y_low)
    # zlow=np.vectorize(get_coverage)(xxlow,yylow)
    # g=interpolate.interp2d(xxlow,yylow,zlow, kind="cubic")
    # zlow_MT=np.vectorize(get_coverage_MT)(xxlow,yylow)
    # g_MT=interpolate.interp2d(xxlow,yylow,zlow_MT, kind="cubic")
"END SLOW PART"

"Plot actual vs interpolated?"
xnew = np.arange(-5.6, 5.6, 0.1)
ynew = np.arange(-5.6, 5.6, 0.1)
znew = np.vectorize(lookup_coverage)(xnew, ynew)
# plt.plot(x, z[0, :], 'ro-', xnew, znew[0, :], 'b-')
# plt.show()
# ""
fig, ax = plt.subplots(1, 1)
ax.set_aspect(1.0 / ax.get_data_ratio())
cp = ax.contour(xnew, ynew, znew)
# xnew = np.arange(-3.2, 3.1, 1e-2)
# ynew = np.arange(-3.2, 3.1, 1e-2)
# znew = f(xnew, ynew)
# plt.plot(x, z[0, :], 'ro-', xnew, znew[0, :], 'b-')
# plt.show()
# fig,ax=plt.subplots(1,1)
# ax.set_aspect(1./ax.get_data_ratio())
# cp = ax.contour(xnew,ynew,znew)
# plt.show()
# fig,ax=plt.subplots(1,1)
# ax.set_aspect(1./ax.get_data_ratio())
# cp = ax.contour(x,y,z)


def lookup_coverage(x, y, res="high"):
    if res == "low":
        z = g(x, y)
    else:
        try:
            z = abs(round(f(x, y), ndigits=3))
        except:
            z = abs(np.around(f(x, y), decimals=3))
    try:
        return z[:, 0]
    except:
        return z


def lookup_coverage_MT(x, y, res="high"):
    if res == "low":
        z = g_MT(x, y)
    else:
        try:
            z = abs(round(f_MT(x, y), ndigits=3))
        except:
            z = abs(np.around(f_MT(x, y), decimals=3))
    try:
        return z[:, 0]
    except:
        return z


# zinterp=lookup_coverage(csv_use[::100,1],csv_use[::100,2], res="low")
# plt.scatter(csv_use[::100,1], zinterp, s=0.2)
# plt.show()


diameters_list = []
mirror_area = {}
duration = {}
z_list = []
dur_list = []
# fig, ax = plt.subplots()
times_list = [[], [], [], []]
times_2_list = [[], [], [], []]
az_hist_list = [[], [], [], []]
for k in [0, 1, 2, 3]:  # range(len(break_up)+1):
    count = 0
    mirror_area[k] = {}
    duration[k] = {}
    for i in x_track_dict[k]:
        az_value_i = az_one_dict[k][i]

        times = date_track_dict[k][i][::10]
        durs = np.diff(times)
        z = lookup_coverage(
            x_track_dict[k][i][::10], y_track_dict[k][i][::10], res="high"
        )
        effective_area = sum(
            np.multiply(z[:-1] * hexagon.area / 100, 1440 * durs)
        ) / sum(
            1440 * durs
        )  # sum(z)/len(z)*hexagon.area
        effective_time = sum(1440 * (durs))
        #            print(effective_area, effective_time)
        mirror_area[k][i] = z
        duration[k][i] = durs
        dur_list.extend(list(durs))
        z_list.append(effective_area)
        times_list[k].append(2 * (np.sqrt(1 / math.pi * effective_area)))
        times_2_list[k].append(
            2 * (np.sqrt(1 / math.pi * effective_area * effective_time))
        )
        if az_value_i <= 45 or az_value_i > 315:
            az_hist_list[0].append(2 * (np.sqrt(1 / math.pi * effective_area)))
        elif az_value_i > 45 and az_value_i <= 135:
            az_hist_list[1].append(2 * (np.sqrt(1 / math.pi * effective_area)))
        elif az_value_i > 135 and az_value_i <= 225:
            az_hist_list[2].append(2 * (np.sqrt(1 / math.pi * effective_area)))
        elif az_value_i > 225 and az_value_i <= 315:
            az_hist_list[3].append(2 * (np.sqrt(1 / math.pi * effective_area)))
        #            ax.scatter(z[:-1]*hexagon.area, 1440*(durs), alpha=0.3, s=0.3)
        #            ax.plot(count,effective_area)#,s=0.5)#, alpha=0.6)

        count += 1
diameters = 2 * (np.sqrt(1 / math.pi * np.array(z_list)))
diameters_list.extend(diameters)
##diameters_list=[]
# ax.plot(diameters, "-o", alpha=0.3)
# ax.set_xlabel("Observation number")
# ax.set_ylabel("Effective diameter (m)")
# ax.axhline(y=2*(np.sqrt(hexagon.area*1/math.pi)), linestyle="--", label="SALT mirror area")
# ax.set_title("Effective observing area for ~80 observations (Dec 2011)")
# ax.legend()
##
##hist, bins=np.histogram(z_list)
##plt.bar(bins[:-1], hist)


plot_this = False
if plot_this == True:
    fig, ax = plt.subplots()
    hist, bins = np.histogram(diameters_list, range=[6.5, 10], bins=35)
    ax.bar(bins[:-1], hist, width=0.5 * (bins[1] - bins[0]), label="")
    ax.set_xlabel("Effective diameter (m)")
    ax.set_ylabel("Observations")
    ax.set_title("Effective diameter per observation \nSALT 201{}".format(year))
    # ax.legend()


"side by side times eff diam hist"
if plot_this == True:
    fig, ax = plt.subplots()
    hist_0, bins_0 = np.histogram(times_list[0], range=[6.5, 10], bins=35)
    hist_1, bins_1 = np.histogram(times_list[1], range=[6.5, 10], bins=35)
    hist_2, bins_2 = np.histogram(times_list[2], range=[6.5, 10], bins=35)
    hist_3, bins_3 = np.histogram(times_list[3], range=[6.5, 10], bins=35)
    # ax.bar(bins[:-1], hist, width=0.5*(bins[1]-bins[0]), label="")
    ax.set_xlabel("Effective diameter (m)")
    ax.set_ylabel("Observations")
    ax.set_title(
        "Effective diameter per observation \nSALT Semester 1, 201{}".format(year)
    )
    # ax.legend()
    # ax = plt.subplot(111)
    w = 0.2 * (bins_0[1] - bins_0[0])
    colors = ['lightblue', 'lightgreen', 'moccasin', 'lightpink', 'lightgrey']
    ax.bar(
        bins_0[:-1] - w, hist_0, width=w, color=colors[0], align='center', linewidth=0
    )
    ax.bar(bins_0[:-1], hist_1, width=w, color=colors[1], align='center', linewidth=0)
    ax.bar(
        bins_0[:-1] + w, hist_2, width=w, color=colors[2], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + 2 * w,
        hist_3,
        width=w,
        color=colors[3],
        align='center',
        linewidth=0,
    )
    ax.autoscale(tight=True)
    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
    labels = ["<20min", "20-40min", "40-60min", ">60min"]
    leg = ax.legend(
        lines,
        labels,
        fancybox=True,
        framealpha=0.5,
        # bbox_to_anchor=(1.05, 1), loc='left',
        borderaxespad=0.0,
        fontsize='xx-small',
    )

"side by side az eff diam hist"
if plot_this == True:
    fig, ax = plt.subplots()
    hist_0, bins_0 = np.histogram(az_hist_list[0], range=[6.5, 10], bins=35)
    hist_1, bins_1 = np.histogram(az_hist_list[1], range=[6.5, 10], bins=35)
    hist_2, bins_2 = np.histogram(az_hist_list[2], range=[6.5, 10], bins=35)
    hist_3, bins_3 = np.histogram(az_hist_list[3], range=[6.5, 10], bins=35)
    ax.set_xlabel("Effective diameter (m)")
    ax.set_ylabel("Observations")
    ax.set_title("Effective diameter by Azimuth \nSALT Semester 1, 201{}".format(year))
    w = 0.2 * (bins_0[1] - bins_0[0])
    colors = ['lightblue', 'lightgreen', 'moccasin', 'lightpink', 'lightgrey']
    ax.bar(
        bins_0[:-1] - w, hist_0, width=w, color=colors[0], align='center', linewidth=0
    )
    ax.bar(bins_0[:-1], hist_1, width=w, color=colors[1], align='center', linewidth=0)
    ax.bar(
        bins_0[:-1] + w, hist_2, width=w, color=colors[2], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + 2 * w,
        hist_3,
        width=w,
        color=colors[3],
        align='center',
        linewidth=0,
    )
    ax.autoscale(tight=True)
    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
    labels = ["315 to 45", "45 to 135", "135 to 225", "225 to 315"]
    leg = ax.legend(
        lines,
        labels,
        fancybox=True,
        framealpha=0.5,
        # bbox_to_anchor=(1.05, 1), loc='left',
        borderaxespad=0.0,
        fontsize='xx-small',
    )


"stacked times eff diam hist"
if plot_this == True:
    fig, ax = plt.subplots()
    hist_0, bins_0 = np.histogram(times_list[0], range=[6.5, 10], bins=35)
    hist_1, bins_1 = np.histogram(times_list[1], range=[6.5, 10], bins=35)
    hist_2, bins_2 = np.histogram(times_list[2], range=[6.5, 10], bins=35)
    hist_3, bins_3 = np.histogram(times_list[3], range=[6.5, 10], bins=35)
    # ax.bar(bins[:-1], hist, width=0.5*(bins[1]-bins[0]), label="")
    ax.set_xlabel("Effective diameter (m)")
    ax.set_ylabel("Observations")
    ax.set_title(
        "Effective diameter per observation \nSALT Semester 1, 201{}".format(year)
    )
    # ax.legend()
    # ax = plt.subplot(111)
    w = 0.5 * (bins_0[1] - bins_0[0])
    colors = ['lightblue', 'lightgreen', 'moccasin', 'lightpink', 'lightgrey']
    ax.bar(bins_0[:-1], hist_0, width=w, color=colors[0], align='center', linewidth=0)
    ax.bar(
        bins_0[:-1],
        hist_1,
        width=w,
        color=colors[1],
        align='center',
        linewidth=0,
        bottom=hist_0,
    )
    ax.bar(
        bins_0[:-1],
        hist_2,
        width=w,
        color=colors[2],
        align='center',
        linewidth=0,
        bottom=hist_1 + hist_0,
    )
    ax.bar(
        bins_0[:-1],
        hist_3,
        width=w,
        color=colors[3],
        align='center',
        linewidth=0,
        bottom=hist_2 + hist_1 + hist_0,
    )
    ax.autoscale(tight=True)
    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
    labels = ["<20min", "20-40min", "40-60min", ">60min"]
    leg = ax.legend(
        lines,
        labels,
        fancybox=True,
        framealpha=0.5,
        # bbox_to_anchor=(1.05, 1), loc='left',
        borderaxespad=0.0,
        fontsize='xx-small',
    )


"tracks, 3 plots by eff d"
write = False
diameters_list = []
mirror_area = {}
duration = {}
z_list = []
dur_list = []
if write == True:
    colors = ['lightblue', 'lightgreen', 'moccasin', 'lightpink', 'lightgrey']
    colors2 = ['b', 'green', 'orange', 'pink', 'grey']
    colors3 = ["black", "black", "black", "black"]
    times_names = ["<20min", "20-40min", "40-60min", ">60min"]
    dates = [
        convert_date("201{}-07-01 12:00:00".format(year)),
        convert_date("201{}-09-01 12:00:00".format(year)),
    ]
    # count=0
    tot_count = 0
    fig1, ax1 = plt.subplots()
    fig2, ax2 = plt.subplots()
    fig3, ax3 = plt.subplots()
    axes = (ax1, ax2, ax3)
    for ax in axes:
        xnew = np.arange(-3.2, 3.1, 1e-2)
        ynew = np.arange(-3.2, 3.1, 1e-2)
        # znew = f(xnew, ynew)
        # ax.set_aspect(1./ax.get_data_ratio())
        znew_diam = 2 * (np.sqrt(1 / math.pi * np.array(znew * hexagon.area / 100)))
        cp = ax.contour(xnew, ynew, znew_diam)
        ax.clabel(
            cp, inline=True, inline_spacing=3, rightside_up=True
        )  # , colors='k', fontsize=8, fmt=fmt)
    for k in [3, 2, 1, 0]:
        count = 0
        count = 0
        mirror_area[k] = {}
        duration[k] = {}
        for i in x_track_dict[k]:
            times = date_track_dict[k][i][::10]
            durs = np.diff(times)
            z = lookup_coverage(
                x_track_dict[k][i][::10], y_track_dict[k][i][::10], res="high"
            )
            effective_area = sum(
                np.multiply(z[:-1] * hexagon.area / 100, 1440 * durs)
            ) / sum(
                1440 * durs
            )  # sum(z)/len(z)*hexagon.area
            effective_time = sum(1440 * (durs))

            dia_i = 2 * (np.sqrt(1 / math.pi * np.array(effective_area)))
            date = [dia_i, dia_i]
            dates = [7.75, 8.5]
            if date[0] < dates[0]:
                ax = ax1
                ax.set_title("201{} Semester 1, Eff Diam <7.75".format(year))
            if date[0] > dates[0] and date[0] < dates[1]:
                ax = ax2
                ax.set_title("201{} Semester 1, Eff Diam >7.75, <8.5".format(year))
            if date[0] > dates[1]:
                ax = ax3
                ax.set_title("201{} Semester 1, Eff Diam >8.5".format(year))
            # if tot_count>1000:
            # break
            if count == 0:
                ax.plot(
                    x_track_dict[k][i][0:-1],
                    y_track_dict[k][i][0:-1],
                    color=colors[k],
                    linewidth=0.2,
                    alpha=0.65,
                    label=times[k],
                )  # , s=0.1)
                ax.scatter(
                    x_track_dict[k][i][0],
                    y_track_dict[k][i][0],
                    color=colors2[k],
                    s=0.2,
                    alpha=(0.95),
                    marker=".",
                )
                # ax.scatter(x_track_dict[k][i][-1], y_track_dict[k][i][-1],
                #     color=colors3[k],s=0.2,alpha=(0.95), marker=".")
            else:
                ax.plot(
                    x_track_dict[k][i][0:-1],
                    y_track_dict[k][i][0:-1],
                    color=colors[k],
                    linewidth=0.2,
                    alpha=(0.65),
                )  # /(k+1)+0.4))#, s=0.1)
                ax.scatter(
                    x_track_dict[k][i][0],
                    y_track_dict[k][i][0],
                    color=colors2[k],
                    s=0.2,
                    alpha=(0.95),
                    marker=".",
                )
                # ax.scatter(x_track_dict[k][i][-1], y_track_dict[k][i][-1],
                #     color=colors3[k],s=0.2,alpha=(0.95), marker=".")
                count += 1
                plt.show()
            count += 1
            tot_count += 1
            # if count==15:
            #     break
            # for ax in axes:

            ax.set_xlabel("x")
            ax.set_ylabel("y")
            ax.set_ylim(-1.75, 1.75)
            ax.set_xlim(1.75, -1.75)
            ax.set_aspect(1.0 / ax.get_data_ratio())
            try:
                lines = [
                    Line2D([0], [0], color=c, linewidth=3, linestyle='-')
                    for c in colors
                ]
                labels = times_names
                # plt.legend(lines, labels)
                leg = ax.legend(
                    lines,
                    labels,
                    fancybox=True,
                    framealpha=0.5,
                    bbox_to_anchor=(1.05, 1),
                    loc='upper left',
                    borderaxespad=0.0,
                    fontsize='xx-small',
                )
                # for l in leg.get_lines():
                #     l.set_linewidth(4.0)
                #     l.set_alpha(1)
            except:
                pass

    # fig.suptitle("Tracks on {} nights in 201{}".format(nights, year))

"dur vs eff d"
write = False
diameters_list = []
mirror_area = {}
duration = {}
z_list = []
dur_list = []
if write == True:
    colors = ['lightblue', 'lightgreen', 'moccasin', 'lightpink', 'lightgrey']
    colors2 = ['b', 'green', 'orange', 'pink', 'grey']
    colors3 = ["black", "black", "black", "black"]
    times_names = ["<20min", "20-40min", "40-60min", ">60min"]
    dates = [
        convert_date("201{}-07-01 12:00:00".format(year)),
        convert_date("201{}-09-01 12:00:00".format(year)),
    ]
    # count=0
    tot_count = 0
    fig1, ax1 = plt.subplots()
    # fig2,ax2=plt.subplots()
    # fig3,ax3=plt.subplots()
    # axes=(ax1,ax2,ax3)
    # for ax in axes:
    #     xnew = np.arange(-3.2, 3.1, 1e-2)
    #     ynew = np.arange(-3.2, 3.1, 1e-2)
    #     # znew = f(xnew, ynew)
    #     # ax.set_aspect(1./ax.get_data_ratio())
    #     znew_diam=2*(np.sqrt(1/math.pi*np.array(znew*hexagon.area/100)))
    #     cp = ax.contour(xnew,ynew,znew_diam)
    #     ax.clabel(cp, inline=True, inline_spacing=3, rightside_up=True)#, colors='k', fontsize=8, fmt=fmt)
    for k in [3, 2, 1, 0]:
        count = 0
        count = 0
        mirror_area[k] = {}
        duration[k] = {}
        for i in x_track_dict[k]:
            times = date_track_dict[k][i][::10]
            durs = np.diff(times)
            z = lookup_coverage(
                x_track_dict[k][i][::10], y_track_dict[k][i][::10], res="high"
            )
            effective_area = sum(
                np.multiply(z[:-1] * hexagon.area / 100, 1440 * durs)
            ) / sum(
                1440 * durs
            )  # sum(z)/len(z)*hexagon.area
            effective_time = sum(1440 * (durs))
            ax = ax1
            az_value_i = az_one_dict[k][i]
            if az_value_i <= 45 or az_value_i > 315:
                color = 'b'
            elif az_value_i > 45 and az_value_i <= 135:
                color = 'g'
            elif az_value_i > 135 and az_value_i <= 225:
                color = 'orange'
            elif az_value_i > 225 and az_value_i <= 315:
                color = 'r'
            dia_i = 2 * (np.sqrt(1 / math.pi * np.array(effective_area)))
            ax.scatter(effective_time, dia_i, color=color, s=2)
            # date=[dia_i,dia_i]
            # dates=[7.75,8.5]
            # if date[0]<dates[0]:
            #     ax=ax1
            #     ax.set_title("201{} Semester 1, Eff Diam <7.75".format(year))
            # if date[0]>dates[0] and date[0]<dates[1]:
            #     ax=ax2
            #     ax.set_title("201{} Semester 1, Eff Diam >7.75, <8.5".format(year))
            # if date[0]>dates[1]:
            #     ax=ax3
            #     ax.set_title("201{} Semester 1, Eff Diam >8.5".format(year))
            # # if tot_count>1000:
            #     # break
            # if count==0:
            #     ax.plot(x_track_dict[k][i][0:-1], y_track_dict[k][i][0:-1],
            #         color=colors[k],linewidth=0.2,alpha=0.65, label=times[k])#, s=0.1)
            #     ax.scatter(x_track_dict[k][i][0], y_track_dict[k][i][0],
            #         color=colors2[k],s=0.2,alpha=(0.95), marker=".")
            #     # ax.scatter(x_track_dict[k][i][-1], y_track_dict[k][i][-1],
            #     #     color=colors3[k],s=0.2,alpha=(0.95), marker=".")
            # else:
            #     ax.plot(x_track_dict[k][i][0:-1], y_track_dict[k][i][0:-1],
            #         color=colors[k],linewidth=0.2,alpha=(0.65))#/(k+1)+0.4))#, s=0.1)
            #     ax.scatter(x_track_dict[k][i][0], y_track_dict[k][i][0],
            #                color=colors2[k],s=0.2,alpha=(0.95), marker=".")
            #     # ax.scatter(x_track_dict[k][i][-1], y_track_dict[k][i][-1],
            #     #     color=colors3[k],s=0.2,alpha=(0.95), marker=".")
            #     count+=1
            #     plt.show()
            count += 1
            tot_count += 1
            # if count==15:
            #     break
            # for ax in axes:
            ax.set_title("Effective Diameter and Track Duration by Azimuth Quadrant")
            ax.set_xlabel("duration (min)")
            ax.set_ylabel("Eff D (m)")
            # ax.set_ylim(-1.75,1.75)
            # ax.set_xlim( 1.75, -1.75)
            # ax.set_aspect(1./ax.get_data_ratio())
            try:
                colors = ["b", "g", "orange", "r"]
                lines = [
                    Line2D([0], [0], color=c, linewidth=3, linestyle='-')
                    for c in colors
                ]
                labels = ["N", "E", "S", "W"]
                # plt.legend(lines, labels)
                leg = ax.legend(
                    lines,
                    labels,
                    fancybox=True,
                    framealpha=0.5,
                    bbox_to_anchor=(1.05, 1),
                    loc='upper left',
                    borderaxespad=0.0,
                    fontsize='xx-small',
                )
                # for l in leg.get_lines():
                #     l.set_linewidth(4.0)
                #     l.set_alpha(1)
            except:
                pass


"tracks, 3 plots by dur"
write = False
diameters_list = []
mirror_area = {}
duration = {}
z_list = []
dur_list = []
if write == True:
    colors = ['lightblue', 'lightgreen', 'moccasin', 'lightpink', 'lightgrey']
    colors2 = ['b', 'green', 'orange', 'pink', 'grey']
    times_names = ["<20min", "20-40min", "40-60min", ">60min"]
    dates = [
        convert_date("201{}-07-01 12:00:00".format(year)),
        convert_date("201{}-09-01 12:00:00".format(year)),
    ]
    # count=0
    tot_count = 0
    fig1, ax1 = plt.subplots()
    fig2, ax2 = plt.subplots()
    fig3, ax3 = plt.subplots()
    fig4, ax4 = plt.subplots()
    axes = (ax1, ax2, ax3, ax4)
    for k in [3, 2, 1, 0]:
        if k == 0:
            ax = ax1

        if k == 1:
            ax = ax2
        if k == 2:
            ax = ax3
        if k == 3:
            ax = ax4
        count = 0
        count = 0
        mirror_area[k] = {}
        duration[k] = {}
        for i in x_track_dict[k]:
            times = date_track_dict[k][i][::10]
            durs = np.diff(times)
            z = lookup_coverage(
                x_track_dict[k][i][::10], y_track_dict[k][i][::10], res="high"
            )
            effective_area = sum(
                np.multiply(z[:-1] * hexagon.area / 100, 1440 * durs)
            ) / sum(
                1440 * durs
            )  # sum(z)/len(z)*hexagon.area
            effective_time = sum(1440 * (durs))

            dia_i = 2 * (np.sqrt(1 / math.pi * np.array(effective_area)))
            date = [dia_i, dia_i]
            dates = [7.75, 8.5]
            colors = ["red", "purple", "blue"]
            if date[0] < dates[0]:
                color_this = colors[0]
            if date[0] > dates[0] and date[0] < dates[1]:
                color_this = colors[1]
            if date[0] > dates[1]:
                color_this = colors[2]
            # if tot_count>1000:
            # break
            if count == 0:
                ax.plot(
                    x_track_dict[k][i][0:-1],
                    y_track_dict[k][i][0:-1],
                    color=color_this,
                    linewidth=0.2,
                    alpha=0.55,
                    label=times[k],
                )  # , s=0.1)
                ax.scatter(
                    x_track_dict[k][i][0],
                    y_track_dict[k][i][0],
                    color=color_this,
                    s=0.2,
                    alpha=(0.95),
                    marker=".",
                )
            else:
                ax.plot(
                    x_track_dict[k][i][0:-1],
                    y_track_dict[k][i][0:-1],
                    color=color_this,
                    linewidth=0.2,
                    alpha=(0.55),
                )  # /(k+1)+0.4))#, s=0.1)
                ax.scatter(
                    x_track_dict[k][i][0],
                    y_track_dict[k][i][0],
                    color=color_this,
                    s=0.2,
                    alpha=(0.95),
                    marker=".",
                )
                count += 1
                plt.show()
            count += 1
            tot_count += 1
            # if count==15:
            #     break
            # for ax in axes:
            ax.set_title("2019 Semester 1, tracks {}".format(times_names[k]))
            ax.set_xlabel("x")
            ax.set_ylabel("y")
            ax.set_ylim(-1.75, 1.75)
            ax.set_xlim(1.75, -1.75)
            ax.set_aspect(1.0 / ax.get_data_ratio())
            try:
                lines = [
                    Line2D([0], [0], color=c, linewidth=3, linestyle='-')
                    for c in colors
                ]
                labels = ["eff D <7.75m", "eff D >7.75m, <8.5m", "eff D >8.5m"]
                # plt.legend(lines, labels)
                leg = ax.legend(
                    lines,
                    labels,
                    fancybox=True,
                    framealpha=0.5,
                    bbox_to_anchor=(1.05, 1),
                    loc='upper left',
                    borderaxespad=0.0,
                    fontsize='xx-small',
                )
                # for l in leg.get_lines():
                #     l.set_linewidth(4.0)
                #     l.set_alpha(1)
            except:
                pass
    # fig.suptitle("Tracks on {} nights in 201{}".format(nights, year))


"radec, 3 plots by dur"
write = False
diameters_list = []
mirror_area = {}
duration = {}
z_list = []
dur_list = []
if write == True:
    colors = ['lightblue', 'lightgreen', 'moccasin', 'lightpink', 'lightgrey']
    colors2 = ['b', 'green', 'orange', 'pink', 'grey']
    times_names = ["<20min", "20-40min", "40-60min", ">60min"]
    dates = [
        convert_date("201{}-07-01 12:00:00".format(year)),
        convert_date("201{}-09-01 12:00:00".format(year)),
    ]
    # count=0
    tot_count = 0
    fig1, ax1 = plt.subplots()
    fig2, ax2 = plt.subplots()
    fig3, ax3 = plt.subplots()
    fig4, ax4 = plt.subplots()
    axes = (ax1, ax2, ax3, ax4)
    for k in [3, 2, 1, 0]:
        if k == 0:
            ax = ax1

        if k == 1:
            ax = ax2
        if k == 2:
            ax = ax3
        if k == 3:
            ax = ax4
        count = 0
        count = 0
        mirror_area[k] = {}
        duration[k] = {}
        for i in x_track_dict[k]:

            try:
                times = date_track_dict[k][i][::10]
                durs = np.diff(times)
                z = lookup_coverage(
                    x_track_dict[k][i][::10], y_track_dict[k][i][::10], res="high"
                )
            except:
                times = date_track_dict[k][i]
                durs = np.diff(times)
                z = lookup_coverage(x_track_dict[k][i], y_track_dict[k][i], res="high")

            effective_area = sum(
                np.multiply(z[:-1] * hexagon.area / 100, 1440 * durs)
            ) / sum(
                1440 * durs
            )  # sum(z)/len(z)*hexagon.area
            effective_time = sum(1440 * (durs))

            dia_i = 2 * (np.sqrt(1 / math.pi * np.array(effective_area)))
            date = [dia_i, dia_i]
            dates = [7.75, 8.5]
            colors = ["red", "purple", "blue"]
            if date[0] < dates[0]:
                color_this = colors[0]
            if date[0] > dates[0] and date[0] < dates[1]:
                color_this = colors[1]
            if date[0] > dates[1]:
                color_this = colors[2]
            # if tot_count>1000:
            # break
            if count == 0:
                ax.plot(
                    dec_one_dict[k][i][0:-1],
                    ra_one_dict[k][i][0:-1],
                    color=color_this,
                    linewidth=0.2,
                    alpha=0.55,
                    label=times[k],
                )  # , s=0.1)
                ax.scatter(
                    dec_one_dict[k][i][0],
                    ra_one_dict[k][i][0],
                    color=color_this,
                    s=0.2,
                    alpha=(0.95),
                    marker=".",
                )
            else:
                ax.plot(
                    dec_one_dict[k][i][0:-1],
                    ra_one_dict[k][i][0:-1],
                    color=color_this,
                    linewidth=0.2,
                    alpha=(0.55),
                )  # /(k+1)+0.4))#, s=0.1)
                ax.scatter(
                    dec_one_dict[k][i][0],
                    ra_one_dict[k][i][0],
                    color=color_this,
                    s=0.2,
                    alpha=(0.95),
                    marker=".",
                )
                count += 1
                plt.show()
            count += 1
            tot_count += 1
            # if count==15:
            #     break
            # for ax in axes:
            ax.set_title("2019 Semester 1, RA DEC {}".format(times_names[k]))
            ax.set_xlabel("dec")
            ax.set_ylabel("ra")
            # ax.set_ylim(-1.75,1.75)
            # ax.set_xlim( 1.75, -1.75)
            ax.set_aspect(1.0 / ax.get_data_ratio())
            try:
                lines = [
                    Line2D([0], [0], color=c, linewidth=3, linestyle='-')
                    for c in colors
                ]
                labels = ["eff D <7.75m", "eff D >7.75m, <8.5m", "eff D >8.5m"]
                # plt.legend(lines, labels)
                leg = ax.legend(
                    lines,
                    labels,
                    fancybox=True,
                    framealpha=0.5,
                    bbox_to_anchor=(1.05, 1),
                    loc='upper left',
                    borderaxespad=0.0,
                    fontsize='xx-small',
                )
                # for l in leg.get_lines():
                #     l.set_linewidth(4.0)
                #     l.set_alpha(1)
            except:
                pass
    # fig.suptitle("Tracks on {} nights in 201{}".format(nights, year))


"MT patrols initial"
tracker_xy = (0, 0)
# tracker_point=Point(tracker_xy[0], tracker_xy[1])
len_sides = 1.6 / 2
r = [0.2, 2]
theta = [60, 210]
MT_centers = [
    Point(tracker_xy[0] + len_sides, tracker_xy[1] + len_sides),
    Point(tracker_xy[0] + len_sides, tracker_xy[1] - len_sides),
    Point(tracker_xy[0] - len_sides, tracker_xy[1] - len_sides),
    Point(tracker_xy[0] - len_sides, tracker_xy[1] + len_sides),
]


def polar_point(origin_point, angle, distance):
    return [
        origin_point.x + math.sin(math.radians(angle)) * distance,
        origin_point.y + math.cos(math.radians(angle)) * distance,
    ]


def sector(center, start_angle, end_angle, radius=r[1], steps=200, inner_rad=r[0]):

    if start_angle > end_angle:
        start_angle = start_angle - 360
    else:
        pass
    step_angle_width = (end_angle - start_angle) / steps
    sector_width = end_angle - start_angle
    segment_vertices = []

    segment_vertices.append(polar_point(center, 0, 0))
    segment_vertices.append(polar_point(center, start_angle, radius))
    for z in range(1, steps):
        segment_vertices.append(
            (polar_point(center, start_angle + z * step_angle_width, radius))
        )
    segment_vertices.append(polar_point(center, start_angle + sector_width, radius))
    segment_vertices.append(polar_point(center, 0, 0))
    circle = Polygon(segment_vertices)
    smaller_circle = center.buffer(inner_rad, resolution=500)
    annulus = circle.difference(smaller_circle)
    return annulus


angles = (60, 210)
sectors = [
    sector(MT_centers[0], angles[0] - 90, angles[1] - 90),
    sector(MT_centers[1], -angles[1] - 90, -angles[0] - 90),
    sector(MT_centers[2], angles[0] + 90, angles[1] + 90),
    sector(MT_centers[3], -angles[1] + 90, -angles[0] + 90),
]
fig, ax = plt.subplots()
ax.set_aspect(1.0 / ax.get_data_ratio())
for sector in sectors:
    x, y = sector.exterior.xy
    ax.plot(x, y)

"MT patrols second"
tracker_xy = (0, 0)
# tracker_point=Point(tracker_xy[0], tracker_xy[1])
len_sides = math.sqrt(2)
r = [0, 2]
theta = [60, 210]
MT_centers = [
    Point(tracker_xy[0] + len_sides, tracker_xy[1] + len_sides),
    Point(tracker_xy[0] + len_sides, tracker_xy[1] - len_sides),
    Point(tracker_xy[0] - len_sides, tracker_xy[1] - len_sides),
    Point(tracker_xy[0] - len_sides, tracker_xy[1] + len_sides),
]


def polar_point(origin_point, angle, distance):
    return [
        origin_point.x + math.sin(math.radians(angle)) * distance,
        origin_point.y + math.cos(math.radians(angle)) * distance,
    ]


def sector(center, start_angle, end_angle, radius=r[1], steps=200, inner_rad=r[0]):

    if start_angle > end_angle:
        start_angle = start_angle - 360
    else:
        pass
    step_angle_width = (end_angle - start_angle) / steps
    sector_width = end_angle - start_angle
    segment_vertices = []

    segment_vertices.append(polar_point(center, 0, 0))
    segment_vertices.append(polar_point(center, start_angle, radius))
    for z in range(1, steps):
        segment_vertices.append(
            (polar_point(center, start_angle + z * step_angle_width, radius))
        )
    segment_vertices.append(polar_point(center, start_angle + sector_width, radius))
    segment_vertices.append(polar_point(center, 0, 0))
    circle = Polygon(segment_vertices)
    smaller_circle = center.buffer(inner_rad, resolution=500)
    annulus = circle.difference(smaller_circle)
    return annulus


angles = (60, 210)
sectors = [
    sector(MT_centers[0], angles[0] - 90, angles[1] - 90),
    sector(MT_centers[1], -angles[1] - 90, -angles[0] - 90),
    sector(MT_centers[2], angles[0] + 90, angles[1] + 90),
    sector(MT_centers[3], -angles[1] + 90, -angles[0] + 90),
]
fig, ax = plt.subplots()
ax.set_aspect(1.0 / ax.get_data_ratio())
for sector in sectors:
    x, y = sector.exterior.xy
    ax.plot(x, y)


write = False
if write == True:
    """new work."""
    fig, (ax1, ax2) = plt.subplots(2, 1)
    colors_MT = ["r", "b", "lightblue", "lightgreen", "g", "black"]
    labels = ["SALT", "MT 1 at 2m", "MT 2 at 2m", "MT 3 at 2m", "MT 4 at 2m", "t=0"]
    minimum_dist_from_tracker = 2
    maximum_dist_from_tracker = 4
    x_y_distance = math.sqrt(0.5 * minimum_dist_from_tracker ** 2)
    maximum_x_y = 1.5
    x_linspace = np.linspace(-maximum_x_y, maximum_x_y, 100)
    y_linspace = np.zeros((100,))
    # x_track, y_track=x_track_dict[2]['1649'][::10,],y_track_dict[2]['1649'][::10,]
    time_linspace = np.linspace(0, 0.0416667, 100)  # one hr in days
    # time_track=date_track_dict[2]['1649'][::10,]
    # time_track=time_track-time_track[0]
    # x_linspace, y_linspace, time_linspace=x_track, y_track, time_track
    time_linspace = time_linspace * 1440
    mt_1_x = x_linspace + x_y_distance
    mt_1_y = y_linspace + x_y_distance
    mt_2_x = x_linspace + x_y_distance
    mt_2_y = y_linspace - x_y_distance
    mt_3_x = x_linspace - x_y_distance
    mt_3_y = y_linspace - x_y_distance
    mt_4_x = x_linspace - x_y_distance
    mt_4_y = y_linspace + x_y_distance
    ax2.grid(linestyle='--')

    ax2.scatter(x_linspace, y_linspace, s=0.1, color=colors_MT[0], label=labels[0])
    ax2.scatter(mt_1_x, mt_1_y, s=0.1, color=colors_MT[1], label=labels[1])
    ax2.scatter(mt_2_x, mt_2_y, s=0.1, color=colors_MT[2], label=labels[2])
    ax2.scatter(mt_3_x, mt_3_y, s=0.1, color=colors_MT[3], label=labels[3])
    ax2.scatter(mt_4_x, mt_4_y, s=0.1, color=colors_MT[4], label=labels[4])
    ax2.scatter(
        [x_linspace[0], mt_1_x[0], mt_2_x[0], mt_3_x[0], mt_4_x[0]],
        [y_linspace[0], mt_1_y[0], mt_2_y[0], mt_3_y[0], mt_4_y[0]],
        color=colors_MT[5],
        label=labels[5],
        s=0.2,
    )
    ax2.set_xlabel("x (m)")
    ax2.set_ylabel("y (m)")
    ax2.set_xlim(-5, 5)
    ax2.set_ylim(-5, 5)
    ax2.set_aspect(1.0 / ax2.get_data_ratio())
    z_salt = np.vectorize(lookup_coverage)(x_linspace, y_linspace)
    z_1 = np.vectorize(lookup_coverage_MT)(mt_1_x, mt_1_y)
    z_2 = np.vectorize(lookup_coverage_MT)(mt_2_x, mt_2_y)
    z_3 = np.vectorize(lookup_coverage_MT)(mt_3_x, mt_3_y)
    z_4 = np.vectorize(lookup_coverage_MT)(mt_4_x, mt_4_y)
    ax1.scatter(
        time_linspace,
        2 * np.sqrt(1 / math.pi * (z_salt * hexagon.area / 100)),
        s=0.1,
        color=colors_MT[0],
        label=labels[0],
    )
    ax1.scatter(
        time_linspace,
        2 * (np.sqrt(1 / math.pi * np.array(z_1 * hexagon.area / 100))),
        s=0.1,
        color=colors_MT[1],
        label=labels[1],
    )
    ax1.scatter(
        time_linspace,
        2 * (np.sqrt(1 / math.pi * np.array(z_2 * hexagon.area / 100))),
        s=0.1,
        color=colors_MT[2],
        label=labels[2],
    )
    ax1.scatter(
        time_linspace,
        2 * (np.sqrt(1 / math.pi * np.array(z_3 * hexagon.area / 100))),
        s=0.1,
        color=colors_MT[3],
        label=labels[3],
    )
    ax1.scatter(
        time_linspace,
        2 * (np.sqrt(1 / math.pi * np.array(z_4 * hexagon.area / 100))),
        s=0.1,
        color=colors_MT[4],
        label=labels[4],
    )
    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors_MT]
    # labels = ["eff D <7.75m","eff D >7.75m, <8.5m","eff D >8.5m"]
    # plt.legend(lines, labels)

    list_areas = [z_salt, z_1, z_2, z_3, z_4]
    durs = np.diff(time_linspace)
    for i in range(5):
        z = list_areas[i]
        eff_area = sum(np.multiply(z[:-1] * hexagon.area / 100, durs)) / sum(durs)
        eff_d = 2 * (np.sqrt(1 / math.pi * eff_area))
        print(eff_area, eff_d)
        # print(eff_d)
        labels[i] += ", tot eff d={:01.1f}m".format(eff_d)

    leg = ax2.legend(
        lines,
        labels,
        fancybox=True,
        framealpha=0.5,
        bbox_to_anchor=(1.05, 1),
        loc='upper left',
        borderaxespad=0.0,
        fontsize='xx-small',
    )
    ax1.set_title("""Effective diameter for a simple track """)
    ax1.set_xlabel("Time (min)")
    ax1.set_ylabel("Effective diameter (m)")
    plt.subplots_adjust(
        top=0.91, bottom=0.07, left=0.079, right=0.978, hspace=0.255, wspace=0.2
    )
    # xlist = np.linspace(-4, 4, 100)
    # ylist = np.linspace(-4, 4, 100)
    # X, Y = np.meshgrid(xlist, ylist)
    # Z = np.vectorize(f_MT)(X,Y)
    # fig,ax=plt.subplots(1,1)
    # ax.set_aspect(1./ax.get_data_ratio())
    # cp = ax2.contour(X,Y, 2*(np.sqrt(1/math.pi*np.array(Z*hexagon.area/100))))
    # ax2.set_xlim(-4,4)
    # ax2.set_ylim(-4,4)
    # ax2.clabel(cp, inline=True, inline_spacing=3, rightside_up=True)#, colors='k', fontsize=8, fmt=fmt)


def get_MT_coords(
    salt_x,
    salt_y,
    dist_from_salt=2,
    angle_from_h=45,
    all_the_same=True,
    MT_1_dist=0,
    MT_1_angle=0,
    MT_2_dist=0,
    MT_2_angle=0,
    MT_3_dist=0,
    MT_3_angle=0,
    MT_4_dist=0,
    MT_4_angle=0,
):
    if all_the_same == True:
        # print("all the same")
        MT_1_dist, MT_2_dist, MT_3_dist, MT_4_dist = (
            dist_from_salt,
            dist_from_salt,
            dist_from_salt,
            dist_from_salt,
        )
        MT_1_angle, MT_2_angle, MT_3_angle, MT_4_angle = (
            angle_from_h,
            angle_from_h,
            angle_from_h,
            angle_from_h,
        )
        # print(MT_1_dist, MT_1_angle)
    mt_1_x = salt_x + MT_1_dist * math.cos(MT_1_angle * math.pi / 180)
    mt_1_y = salt_y + MT_1_dist * math.sin(MT_1_angle * math.pi / 180)
    mt_2_x = salt_x + MT_2_dist * math.cos(MT_2_angle * math.pi / 180)
    mt_2_y = salt_y - MT_2_dist * math.sin(MT_2_angle * math.pi / 180)
    mt_3_x = salt_x - MT_3_dist * math.cos(MT_3_angle * math.pi / 180)
    mt_3_y = salt_y - MT_3_dist * math.sin(MT_3_angle * math.pi / 180)
    mt_4_x = salt_x - MT_4_dist * math.cos(MT_4_angle * math.pi / 180)
    mt_4_y = salt_y + MT_4_dist * math.sin(MT_4_angle * math.pi / 180)
    return (mt_1_x, mt_1_y), (mt_2_x, mt_2_y), (mt_3_x, mt_3_y), (mt_4_x, mt_4_y)


def calc_eff_d_t(x, y, tracker="SALT"):
    if tracker == "SALT":
        z = np.vectorize(lookup_coverage)(x, y)
    else:
        # print("using MT fov")
        z = np.vectorize(lookup_coverage_MT)(x, y)
        # print(z.shape)
    eff_d_t = np.around(2 * np.sqrt(1 / math.pi * (z * hexagon.area / 100)), decimals=3)
    return eff_d_t


def tot_eff_d(eff_d_t, times, tracker="MT"):
    if tracker == "MT":
        eff_d_t_use = np.around(eff_d_t[np.logical_not(eff_d_t < 3.5)], decimals=3)
        if len(eff_d_t_use) != 0:
            durs = np.around(
                np.gradient(times)[np.logical_not(eff_d_t < 3.5)], decimals=3
            )
            # tot_dur=times[-1]-times[0]
            eff_d_tot = np.around(
                sum(np.multiply(eff_d_t_use, durs)) / sum(durs), decimals=3
            )
            return eff_d_tot
        else:
            return 0
    else:  # don't filter out <3.5m
        eff_d_t_use = np.around(eff_d_t, decimals=3)
        if len(eff_d_t_use) != 0:
            durs = np.around(np.gradient(times), decimals=3)
            # tot_dur=times[-1]-times[0]
            eff_d_tot = np.around(
                sum(np.multiply(eff_d_t_use, durs)) / sum(durs), decimals=3
            )
            return eff_d_tot
        else:
            return 0


def tot_time(eff_d_t, times, tracker="MT"):
    if tracker == "MT":
        eff_d_t_use = np.around(eff_d_t[np.logical_not(eff_d_t < 3.5)], decimals=3)
        if len(eff_d_t_use) != 0:
            # durs=np.around(np.gradient(times[np.logical_not(eff_d_t<3.5)]), decimals=3)
            # times_okay=np.around(times[np.logical_not(eff_d_t<3.5)], decimals=3)
            # print(np.diff(times).shape)
            # print([np.logical_not(eff_d_t<3.5)][:-1])
            times_diff = np.diff(times)[np.logical_not(eff_d_t < 3.5)[:-1]]
            # tot_dur=times_okay[-1]-times_okay[0]
            tot_dur = np.around(sum(times_diff), decimals=3)
            # eff_d_tot=np.around(sum(np.multiply(eff_d_t_use, durs))/sum(durs),decimals=3)
            return tot_dur
        else:
            return 0
    else:  # don't filter out <3.5m
        eff_d_t_use = np.around(eff_d_t, decimals=3)
        if len(eff_d_t_use) != 0:
            times_diff = np.diff(times)
            tot_dur = np.around(sum(times_diff), decimals=3)
            return tot_dur
        else:
            return 0


def get_real_tracks(
    x_track_dict, y_track_dict, date_track_dict, MT_dist=2, number_of_tracks=10
):
    count, fail_count = 0, 0
    if number_of_tracks == 10:
        keys = [1, 2, 3]
    else:
        keys = [0, 1, 2, 3]
    tot_salt, tot_1, tot_2, tot_3, tot_4 = [], [], [], [], []
    time_salt, time_1, time_2, time_3, time_4 = [], [], [], [], []
    # while (count<number_of_tracks):
    for k in keys:
        for i in x_track_dict[k]:
            try:
                print(count)
                x_track, y_track = (
                    x_track_dict[k][i][
                        ::10,
                    ],
                    y_track_dict[k][i][
                        ::10,
                    ],
                )
                time_track = date_track_dict[k][i][
                    ::10,
                ]
                time_track = time_track - time_track[0]
                x_linspace, y_linspace, time_linspace = x_track, y_track, time_track
                time_linspace = time_linspace * 1440
                mt1xy, mt2xy, mt3xy, mt4xy = get_MT_coords(
                    x_linspace, y_linspace, dist_from_salt=MT_dist
                )
                effdt_salt = calc_eff_d_t(x_linspace, y_linspace)
                effdt1 = calc_eff_d_t(mt1xy[0], mt1xy[1], tracker="MT")
                effdt2 = calc_eff_d_t(mt2xy[0], mt2xy[1], tracker="MT")
                effdt3 = calc_eff_d_t(mt3xy[0], mt3xy[1], tracker="MT")
                effdt4 = calc_eff_d_t(mt4xy[0], mt4xy[1], tracker="MT")
                # plt.scatter(time_linspace,effdt_salt,s=0.1)
                # plt.scatter(time_linspace, effdt1, s=0.1)
                tot_salt.append(tot_eff_d(effdt_salt, time_linspace, tracker="SALT"))
                tot_1.append(tot_eff_d(effdt1, time_linspace))
                tot_2.append(tot_eff_d(effdt2, time_linspace))
                tot_3.append(tot_eff_d(effdt3, time_linspace))
                tot_4.append(tot_eff_d(effdt4, time_linspace))
                time_salt.append(tot_time(effdt_salt, time_linspace, tracker="SALT"))
                time_1.append(tot_time(effdt1, time_linspace))
                time_2.append(tot_time(effdt2, time_linspace))
                time_3.append(tot_time(effdt3, time_linspace))
                time_4.append(tot_time(effdt4, time_linspace))
                count += 1
            except:
                fail_count += 1
                count += 1

    return (
        tot_salt,
        tot_1,
        tot_2,
        tot_3,
        tot_4,
        time_salt,
        time_1,
        time_2,
        time_3,
        time_4,
        fail_count,
    )


def remove_under_20(MT_list, mt_durs):
    mt_array = np.array(mt_durs)
    mt_ma = np.ma.array(mt_array, mask=np.less(mt_array, 20))
    mt_zeroed = mt_ma.filled(fill_value=0)
    return mt_zeroed


def remove_all(list_eff, list_dur):
    new_list_eff, new_list_dur = [], []
    for i in range(len(list_eff)):
        new_list_eff.append(remove_under_20(list_eff[i], list_dur[i]))
        new_list_dur.append(remove_under_20(list_dur[i], list_dur[i]))
    return new_list_eff, new_list_dur


def de_zero(array):
    array_sure = np.array(array)
    no_zero = array_sure[np.not_equal(array_sure, 0)]
    return no_zero


write = False
"testing functions"
if write == True:
    x_linspace = np.linspace(-maximum_x_y, maximum_x_y, 100)
    y_linspace = np.zeros((100,))
    time_linspace = np.linspace(0, 0.0416667, 100)  # one hr in days
    time_linspace = time_linspace * 1440
    mt1xy, mt2xy, mt3xy, mt4xy = get_MT_coords(x_linspace, y_linspace, dist_from_salt=4)
    effdt_salt = calc_eff_d_t(x_linspace, y_linspace)
    effdt1 = calc_eff_d_t(mt1xy[0], mt1xy[1], tracker="MT")
    effdt2 = calc_eff_d_t(mt2xy[0], mt2xy[1], tracker="MT")
    effdt3 = calc_eff_d_t(mt3xy[0], mt3xy[1], tracker="MT")
    # plt.scatter(time_linspace,effdt_salt,s=0.1)
    # plt.scatter(time_linspace, effdt1, s=0.1)
    effdtot_salt = tot_eff_d(effdt_salt, time_linspace)
    effdtot1 = tot_eff_d(effdt1, time_linspace)
    effdtot2 = tot_eff_d(effdt2, time_linspace)
    effdtot3 = tot_eff_d(effdt3, time_linspace)
    print(effdtot_salt, effdtot1, effdtot2, effdtot3)


"get real tracks in MTs"
write = False
if write == True:
    print(1)
    "2m: tot eff d and dur, >3.5m, >20min"
    # tot_salt,tot_1,tot_2,tot_3,tot_4, time_salt, time_1, time_2, time_3, time_4,fail_count= get_real_tracks(
    #     x_track_dict,y_track_dict,date_track_dict,MT_dist=2,number_of_tracks=10)
    "2.5m: tot eff d and dur, >3.5m, >20min"
    # tot_salt_25,tot_1_25,tot_2_25,tot_3_25,tot_4_25, time_salt_25, time_1_25, time_2_25, time_3_25, time_4_25,fail_count_25= get_real_tracks(
    #     x_track_dict,y_track_dict,date_track_dict,MT_dist=2.5,number_of_tracks=10)
    "3m: tot eff d and dur, >3.5m, >20min"
    # tot_salt_3m,tot_1_3m,tot_2_3m,tot_3_3m,tot_4_3m, time_salt_3m, time_1_3m, time_2_3m, time_3_3m, time_4_3m,fail_count_3m= get_real_tracks(
    #     x_track_dict,y_track_dict,date_track_dict,MT_dist=3,number_of_tracks=10)
    "3.5m: tot eff d and dur, >3.5m, >20min"
    # tot_salt_35,tot_1_35,tot_2_35,tot_3_35,tot_4_35, time_salt_35, time_1_35, time_2_35, time_3_35, time_4_35,fail_count_35= get_real_tracks(
    #     x_track_dict,y_track_dict,date_track_dict,MT_dist=3.5,number_of_tracks=10)
    "3.75m: tot eff d and dur, >3.5m, >20min"
    # tot_salt_375,tot_1_375,tot_2_375,tot_3_375,tot_4_375, time_salt_375, time_1_375, time_2_375, time_3_375, time_4_375,fail_count_375= get_real_tracks(
    #     x_track_dict,y_track_dict,date_track_dict,MT_dist=3.75,number_of_tracks=10)
    "4m: tot eff d and dur, >3.5m, >20min"
    # tot_salt_4,tot_1_4,tot_2_4,tot_3_4,tot_4_4,time_salt_4,time_1_4,time_2_4,time_3_4,time_4_4, fail_count= get_real_tracks(
    #     x_track_dict,y_track_dict,date_track_dict,MT_dist=4,number_of_tracks=10)
    "2m: tot eff d and dur, >3.5m, incl 20min"
    # all_tot_salt,all_tot_1,all_tot_2,all_tot_3,all_tot_4, all_time_salt, all_time_1, all_time_2, all_time_3, all_time_4,fail_count= get_real_tracks(
    #     x_track_dict,y_track_dict,date_track_dict,MT_dist=2,number_of_tracks=0)
    "4m: tot eff d and dur, >3.5m, incl 20min"
    # all_tot_salt_4,all_tot_1_4,all_tot_2_4,all_tot_3_4,all_tot_4_4,all_time_salt_4,all_time_1_4,all_time_2_4,all_time_3_4,all_time_4_4, fail_count= get_real_tracks(
    #     x_track_dict,y_track_dict,date_track_dict,MT_dist=4,number_of_tracks=0)
    "salt >20min, MT zero when <20min 2m"
    list_eff_2 = [all_tot_1, all_tot_2, all_tot_3, all_tot_4]
    list_dur_2 = [all_time_1, all_time_2, all_time_3, all_time_4]
    new_list_eff_2, new_list_dur_2 = remove_all(list_eff_2, list_dur_2)
    dur_no_20_2_1, dur_no_20_2_2, dur_no_20_2_3, dur_no_20_2_4 = (
        new_list_dur_2[0],
        new_list_dur_2[1],
        new_list_dur_2[2],
        new_list_dur_2[3],
    )
    "salt >20min, MT zero when <20min 4m"
    list_eff_4 = [all_tot_1_4, all_tot_2_4, all_tot_3_4, all_tot_4_4]
    list_dur_4 = [all_time_1_4, all_time_2_4, all_time_3_4, all_time_4_4]
    new_list_eff_4, new_list_dur_4 = remove_all(list_eff_4, list_dur_4)
    dur_no_20_4_salt = all_time_salt_4
    diam_no_20_4_salt = all_tot_salt_4
    dur_no_20_4_1, dur_no_20_4_2, dur_no_20_4_3, dur_no_20_4_4 = (
        new_list_dur_4[0],
        new_list_dur_4[1],
        new_list_dur_4[2],
        new_list_dur_4[3],
    )
    diam_no_20_4_1, diam_no_20_4_2, diam_no_20_4_3, diam_no_20_4_4 = (
        new_list_eff_4[0],
        new_list_eff_4[1],
        new_list_eff_4[2],
        new_list_eff_4[3],
    )

"hist diam 4m"
if write == True:
    hist_salt, bins_salt = np.histogram(tot_salt, range=[3.5, 9.5], bins=30)
    hist_1, bins_1 = np.histogram(tot_1_4, range=[3.5, 9.5], bins=30)
    hist_2, bins_2 = np.histogram(tot_2_4, range=[3.5, 9.5], bins=30)
    hist_3, bins_3 = np.histogram(tot_3_4, range=[3.5, 9.5], bins=30)
    hist_4, bins_4 = np.histogram(tot_4_4, range=[3.5, 9.5], bins=30)
    fig, ax = plt.subplots()
    # ax.bar(bins[:-1], hist, width=0.5*(bins[1]-bins[0]), label="")
    ax.set_xlabel("Effective diameter (m)")
    ax.set_ylabel("Number of tracks")
    ax.set_title(
        "Effective diameter per observation 2019 \n tracks >20min and using MT aperture(t)>3.5m"
    )
    # ax.legend()
    # ax = plt.subplot(111)
    bins_0 = bins_salt
    w = 0.2 * (bins_0[1] - bins_0[0])
    colors = ["r", "b", "lightskyblue", "lightgreen", "g"]
    labels = ["SALT", "MT 1 at 4m", "MT 2 at 4m", "MT 3 at 4m", "MT 4 at 4m"]
    ax.bar(
        bins_0[:-1], hist_salt, width=w, color=colors[0], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + w, hist_1, width=w, color=colors[1], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + 2 * w,
        hist_2,
        width=w,
        color=colors[2],
        align='center',
        linewidth=0,
    )
    ax.bar(
        bins_0[:-1] - w, hist_3, width=w, color=colors[3], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] - 2 * w,
        hist_4,
        width=w,
        color=colors[4],
        align='center',
        linewidth=0,
    )
    ax.autoscale(tight=True)
    ax.set_xlim(3, 9.6)
    ax.set_ylim(0, max(hist_salt) + 10)
    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
    leg = ax.legend(
        lines,
        labels,
        fancybox=True,
        framealpha=0.5,
        # bbox_to_anchor=(1.05, 1), loc='left',
        borderaxespad=0.0,
        fontsize='xx-small',
    )

"hist diam 2m"
if write == True:
    hist_salt, bins_salt = np.histogram(tot_salt, range=[3.5, 9.5], bins=30)
    hist_1, bins_1 = np.histogram(tot_1, range=[3.5, 9.5], bins=30)
    hist_2, bins_2 = np.histogram(tot_2, range=[3.5, 9.5], bins=30)
    hist_3, bins_3 = np.histogram(tot_3, range=[3.5, 9.5], bins=30)
    hist_4, bins_4 = np.histogram(tot_4, range=[3.5, 9.5], bins=30)
    fig, ax = plt.subplots()
    # ax.bar(bins[:-1], hist, width=0.5*(bins[1]-bins[0]), label="")
    ax.set_xlabel("Effective diameter (m)")
    ax.set_ylabel("Number of tracks")
    ax.set_title(
        "Effective diameter per observation 2019 \n tracks >20min and using MT aperture(t)>3.5m"
    )
    # ax.legend()
    # ax = plt.subplot(111)
    bins_0 = bins_salt
    w = 0.2 * (bins_0[1] - bins_0[0])
    dist = 4
    colors = ["r", "b", "lightskyblue", "lightgreen", "g"]
    labels = [
        "SALT",
        "MT 1 at {}m".format(dist),
        "MT 2 at {}m".format(dist),
        "MT 3 at {}m".format(dist),
        "MT 4 at {}m".format(dist),
    ]
    ax.bar(
        bins_0[:-1], hist_salt, width=w, color=colors[0], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + w, hist_1, width=w, color=colors[1], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + 2 * w,
        hist_2,
        width=w,
        color=colors[2],
        align='center',
        linewidth=0,
    )
    ax.bar(
        bins_0[:-1] - w, hist_3, width=w, color=colors[3], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] - 2 * w,
        hist_4,
        width=w,
        color=colors[4],
        align='center',
        linewidth=0,
    )
    ax.autoscale(tight=True)
    ax.set_xlim(3, 9.6)
    ax.set_ylim(0, max(hist_salt) + 10)
    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
    leg = ax.legend(
        lines,
        labels,
        fancybox=True,
        framealpha=0.5,
        # bbox_to_anchor=(1.05, 1), loc='left',
        borderaxespad=0.0,
        fontsize='xx-small',
    )


"time hist 4m"
if write == True:
    hist_salt, bins_salt = np.histogram(
        np.around(time_salt_4, decimals=0), range=[0, 121], bins=24
    )
    hist_1, bins_1 = np.histogram(time_1_4, range=[0, 121], bins=24)
    hist_2, bins_2 = np.histogram(time_2_4, range=[0, 121], bins=24)
    hist_3, bins_3 = np.histogram(time_3_4, range=[0, 121], bins=24)
    hist_4, bins_4 = np.histogram(time_4_4, range=[0, 121], bins=24)
    fig, ax = plt.subplots()
    # ax.bar(bins[:-1], hist, width=0.5*(bins[1]-bins[0]), label="")
    ax.set_xlabel("Track duration (min)")
    ax.set_ylabel("Number of tracks")
    ax.set_title(
        "Track duration per observation 2019 \n tracks >20min and using MT aperture(t)>3.5m"
    )
    # ax.legend()
    # ax = plt.subplot(111)
    bins_0 = bins_salt
    w = 0.15 * (bins_0[1] - bins_0[0])
    colors = ["r", "b", "lightskyblue", "lightgreen", "g"]
    labels = ["SALT", "MT 1 at 4m", "MT 2 at 4m", "MT 3 at 4m", "MT 4 at 4m"]
    ax.bar(
        bins_0[:-1], hist_salt, width=w, color=colors[0], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + w, hist_1, width=w, color=colors[1], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + 2 * w,
        hist_2,
        width=w,
        color=colors[2],
        align='center',
        linewidth=0,
    )
    ax.bar(
        bins_0[:-1] - w, hist_3, width=w, color=colors[3], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] - 2 * w,
        hist_4,
        width=w,
        color=colors[4],
        align='center',
        linewidth=0,
    )
    ax.autoscale(tight=True)
    ax.set_xlim(1, 121)
    ax.set_ylim(0, max(hist_4) + 10)
    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
    leg = ax.legend(
        lines,
        labels,
        fancybox=True,
        framealpha=0.5,
        # bbox_to_anchor=(1.05, 1), loc='left',
        borderaxespad=0.0,
        fontsize='xx-small',
    )

"time hist 2m"
if write == True:
    hist_salt, bins_salt = np.histogram(
        np.around(time_salt, decimals=0), range=[0, 120], bins=12
    )
    hist_1, bins_1 = np.histogram(time_1, range=[0, 120], bins=12)
    hist_2, bins_2 = np.histogram(time_2, range=[0, 120], bins=12)
    hist_3, bins_3 = np.histogram(time_3, range=[0, 120], bins=12)
    hist_4, bins_4 = np.histogram(time_4, range=[0, 120], bins=12)
    fig, ax = plt.subplots()
    # ax.bar(bins[:-1], hist, width=0.5*(bins[1]-bins[0]), label="")
    ax.set_xlabel("Track duration (min)")
    ax.set_ylabel("Number of tracks")
    ax.set_title(
        "Track duration per observation 2019 \n tracks >20min and using MT aperture(t)>3.5m"
    )
    # ax.legend()
    # ax = plt.subplot(111)
    bins_0 = bins_salt
    w = 0.15 * (bins_0[1] - bins_0[0])
    colors = ["r", "b", "lightskyblue", "lightgreen", "g"]
    labels = ["SALT", "MT 1 at 2m", "MT 2 at 2m", "MT 3 at 2m", "MT 4 at 2m"]
    ax.bar(
        bins_0[:-1], hist_salt, width=w, color=colors[0], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + w, hist_1, width=w, color=colors[1], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + 2 * w,
        hist_2,
        width=w,
        color=colors[2],
        align='center',
        linewidth=0,
    )
    ax.bar(
        bins_0[:-1] - w, hist_3, width=w, color=colors[3], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] - 2 * w,
        hist_4,
        width=w,
        color=colors[4],
        align='center',
        linewidth=0,
    )
    ax.autoscale(tight=True)
    # ax.set_xlim(3,9.6)
    ax.set_ylim(0, 1024 + 10)  # to scale to 4m graph
    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
    leg = ax.legend(
        lines,
        labels,
        fancybox=True,
        framealpha=0.5,
        # bbox_to_anchor=(1.05, 1), loc='left',
        borderaxespad=0.0,
        fontsize='xx-small',
    )

"time hist salt incl 20min (MT>3.5m >20min) 4m"
if write == True:
    hist_salt, bins_salt = np.histogram(
        np.around(all_time_salt_4, decimals=0), range=[0, 121], bins=24
    )
    hist_1, bins_1 = np.histogram(all_time_1_4, range=[20, 121], bins=20)
    hist_2, bins_2 = np.histogram(all_time_2_4, range=[20, 121], bins=20)
    hist_3, bins_3 = np.histogram(all_time_3_4, range=[20, 121], bins=20)
    hist_4, bins_4 = np.histogram(all_time_4_4, range=[20, 121], bins=20)
    fig, ax = plt.subplots()
    # ax.bar(bins[:-1], hist, width=0.5*(bins[1]-bins[0]), label="")
    ax.set_xlabel("Track duration (min)")
    ax.set_ylabel("Number of tracks")
    ax.set_title(
        "Track duration per observation 2019 \n MT aperture(t)>3.5m, MT tracks >20min"
    )
    # ax.legend()
    # ax = plt.subplot(111)
    bins_0 = bins_1
    w = 0.15 * (bins_0[1] - bins_0[0])
    colors = ["r", "b", "lightskyblue", "lightgreen", "g"]
    labels = ["SALT", "MT 1 at 4m", "MT 2 at 4m", "MT 3 at 4m", "MT 4 at 4m"]
    ax.bar(
        bins_salt[:-1], hist_salt, width=w, color=colors[0], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + w, hist_1, width=w, color=colors[1], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + 2 * w,
        hist_2,
        width=w,
        color=colors[2],
        align='center',
        linewidth=0,
    )
    ax.bar(
        bins_0[:-1] - w, hist_3, width=w, color=colors[3], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] - 2 * w,
        hist_4,
        width=w,
        color=colors[4],
        align='center',
        linewidth=0,
    )
    ax.autoscale(tight=True)
    # ax.set_xlim(3,9.6)
    ax.set_ylim(0, max(hist_salt) + 10)
    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
    leg = ax.legend(
        lines,
        labels,
        fancybox=True,
        framealpha=0.5,
        # bbox_to_anchor=(1.05, 1), loc='left',
        borderaxespad=0.0,
        fontsize='xx-small',
    )

"time hist salt incl 20min (MT>3.5m >20min) 2m"
if write == True:
    hist_salt, bins_salt = np.histogram(
        np.around(all_time_salt, decimals=0), range=[0, 121], bins=24
    )
    hist_1, bins_1 = np.histogram(all_time_1, range=[20, 121], bins=20)
    hist_2, bins_2 = np.histogram(all_time_2, range=[20, 121], bins=20)
    hist_3, bins_3 = np.histogram(all_time_3, range=[20, 121], bins=20)
    hist_4, bins_4 = np.histogram(all_time_4, range=[20, 121], bins=20)
    fig, ax = plt.subplots()
    # ax.bar(bins[:-1], hist, width=0.5*(bins[1]-bins[0]), label="")
    ax.set_xlabel("Track duration (min)")
    ax.set_ylabel("Number of tracks")
    ax.set_title(
        "Track duration per observation 2019 \n MT aperture(t)>3.5m, MT tracks >20min"
    )
    # ax.legend()
    # ax = plt.subplot(111)
    bins_0 = bins_1
    w = 0.15 * (bins_0[1] - bins_0[0])
    colors = ["r", "b", "lightskyblue", "lightgreen", "g"]
    labels = ["SALT", "MT 1 at 2m", "MT 2 at 2m", "MT 3 at 2m", "MT 4 at 2m"]
    ax.bar(
        bins_salt[:-1], hist_salt, width=w, color=colors[0], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + w, hist_1, width=w, color=colors[1], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + 2 * w,
        hist_2,
        width=w,
        color=colors[2],
        align='center',
        linewidth=0,
    )
    ax.bar(
        bins_0[:-1] - w, hist_3, width=w, color=colors[3], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] - 2 * w,
        hist_4,
        width=w,
        color=colors[4],
        align='center',
        linewidth=0,
    )
    ax.autoscale(tight=True)
    # ax.set_xlim(3,9.6)
    ax.set_ylim(0, max(hist_salt) + 10)
    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
    leg = ax.legend(
        lines,
        labels,
        fancybox=True,
        framealpha=0.5,
        # bbox_to_anchor=(1.05, 1), loc='left',
        borderaxespad=0.0,
        fontsize='xx-small',
    )

"time frac hist incl 20min 2m (MT>3.5 >20min)"
if write == True:
    # new_list_eff_2
    dur_no_20_2_1, dur_no_20_2_2, dur_no_20_2_3, dur_no_20_2_4 = (
        new_list_dur_2[0],
        new_list_dur_2[1],
        new_list_dur_2[2],
        new_list_dur_2[3],
    )
    hist_salt, bins_salt = np.histogram(
        np.divide(all_time_salt, all_time_salt), range=[0, 1.05], bins=20
    )
    hist_1, bins_1 = np.histogram(
        np.divide(dur_no_20_2_1, all_time_salt), range=[0, 1.05], bins=20
    )
    hist_2, bins_2 = np.histogram(
        np.divide(dur_no_20_2_2, all_time_salt), range=[0, 1.05], bins=20
    )
    hist_3, bins_3 = np.histogram(
        np.divide(dur_no_20_2_3, all_time_salt), range=[0, 1.05], bins=20
    )
    hist_4, bins_4 = np.histogram(
        np.divide(dur_no_20_2_4, all_time_salt), range=[0, 1.05], bins=20
    )
    fig, ax = plt.subplots()
    # ax.bar(bins[:-1], hist, width=0.5*(bins[1]-bins[0]), label="")
    ax.set_xlabel("Fraction of SALT's track time")
    ax.set_ylabel("Number of tracks")
    ax.set_title(
        "MT track duration as a fraction of SALT's \n MT aperture(t)>3.5m, MT tracks >20min"
    )
    # ax.legend()
    # ax = plt.subplot(111)
    bins_0 = bins_salt
    w = 0.15 * (bins_0[1] - bins_0[0])
    colors = ["r", "b", "lightskyblue", "lightgreen", "g"]
    labels = ["SALT", "MT 1 at 2m", "MT 2 at 2m", "MT 3 at 2m", "MT 4 at 2m"]
    ax.bar(
        bins_0[:-1] - 2 * w,
        hist_salt,
        width=w,
        color=colors[0],
        align='center',
        linewidth=0,
    )
    ax.bar(
        bins_0[:-1] + w, hist_1, width=w, color=colors[1], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + 2 * w,
        hist_2,
        width=w,
        color=colors[2],
        align='center',
        linewidth=0,
    )
    ax.bar(
        bins_0[:-1] - w, hist_3, width=w, color=colors[3], align='center', linewidth=0
    )
    ax.bar(bins_0[:-1], hist_4, width=w, color=colors[4], align='center', linewidth=0)
    ax.autoscale(tight=True)
    # ax.set_xlim(3,9.6)
    ax.set_ylim(0, max(hist_salt) + 100)
    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
    leg = ax.legend(
        lines,
        labels,
        fancybox=True,
        framealpha=0.5,
        # bbox_to_anchor=(1.05, 1), loc='left',
        borderaxespad=0.0,
        fontsize='xx-small',
    )

"time frac hist incl 20min 4m (MT >3.5 >20min)"
if write == True:
    # new_list_eff_2
    hist_salt, bins_salt = np.histogram(
        np.divide(all_time_salt_4, all_time_salt_4), range=[0, 1.05], bins=20
    )
    hist_1, bins_1 = np.histogram(
        np.divide(dur_no_20_4_1, all_time_salt_4), range=[0, 1.05], bins=20
    )
    hist_2, bins_2 = np.histogram(
        np.divide(dur_no_20_4_2, all_time_salt_4), range=[0, 1.05], bins=20
    )
    hist_3, bins_3 = np.histogram(
        np.divide(dur_no_20_4_3, all_time_salt_4), range=[0, 1.05], bins=20
    )
    hist_4, bins_4 = np.histogram(
        np.divide(dur_no_20_4_4, all_time_salt_4), range=[0, 1.05], bins=20
    )
    fig, ax = plt.subplots()
    # ax.bar(bins[:-1], hist, width=0.5*(bins[1]-bins[0]), label="")
    ax.set_xlabel("Fraction of SALT's track time")
    ax.set_ylabel("Number of tracks")
    ax.set_title(
        "MT track duration as a fraction of SALT's \n MT aperture(t)>3.5m, MT tracks >20min"
    )
    # ax.legend()
    # ax = plt.subplot(111)
    bins_0 = bins_salt
    w = 0.15 * (bins_0[1] - bins_0[0])
    colors = ["r", "b", "lightskyblue", "lightgreen", "g"]
    labels = ["SALT", "MT 1 at 4m", "MT 2 at 4m", "MT 3 at 4m", "MT 4 at 4m"]
    ax.bar(
        bins_0[:-1] - 2 * w,
        hist_salt,
        width=w,
        color=colors[0],
        align='center',
        linewidth=0,
    )
    ax.bar(
        bins_0[:-1] + w, hist_1, width=w, color=colors[1], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + 2 * w,
        hist_2,
        width=w,
        color=colors[2],
        align='center',
        linewidth=0,
    )
    ax.bar(
        bins_0[:-1] - w, hist_3, width=w, color=colors[3], align='center', linewidth=0
    )
    ax.bar(bins_0[:-1], hist_4, width=w, color=colors[4], align='center', linewidth=0)
    ax.autoscale(tight=True)
    # ax.set_xlim(3,9.6)
    ax.set_ylim(0, max(hist_salt) + 100)
    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
    leg = ax.legend(
        lines,
        labels,
        fancybox=True,
        framealpha=0.5,
        # bbox_to_anchor=(1.05, 1), loc='left',
        borderaxespad=0.0,
        fontsize='xx-small',
    )

"comparison of mt salt track time (MT >3.5 >20min) 4m"
if write == True:
    fig, ax = plt.subplots()
    dist = 4
    colors = ["r", "b", "lightskyblue", "lightgreen", "g"]
    labels = [
        "SALT",
        "MT 1 at {}m".format(dist),
        "MT 2 at {}m".format(dist),
        "MT 3 at {}m".format(dist),
        "MT 4 at {}m".format(dist),
    ]
    ax.scatter(
        all_time_salt_4,
        np.divide(dur_no_20_4_1, all_time_salt_4),
        s=0.1,
        color=colors[1],
        alpha=0.5,
    )
    ax.scatter(
        all_time_salt_4,
        np.divide(dur_no_20_4_2, all_time_salt_4),
        s=0.1,
        color=colors[2],
        alpha=0.5,
    )
    ax.scatter(
        all_time_salt_4,
        np.divide(dur_no_20_4_3, all_time_salt_4),
        s=0.1,
        color=colors[3],
        alpha=0.5,
    )
    ax.scatter(
        all_time_salt_4,
        np.divide(dur_no_20_4_4, all_time_salt_4),
        s=0.1,
        color=colors[4],
        alpha=0.5,
    )

"effd^2*dur incl 20min 4m (MT >3.5 )"
if write == True:
    # new_list_eff_2
    hist_salt, bins_salt = np.histogram(
        np.multiply(
            np.square(0.5 * np.array(all_tot_salt_4)), np.array(all_time_salt_4)
        ),
        range=[0, 1575],
        bins=20,
    )
    hist_1, bins_1 = np.histogram(
        np.multiply(np.square(0.5 * np.array(all_tot_1_4)), np.array(all_time_1_4)),
        range=[0, 1575],
        bins=20,
    )
    hist_2, bins_2 = np.histogram(
        np.multiply(np.square(0.5 * np.array(all_tot_2_4)), np.array(all_time_2_4)),
        range=[0, 1575],
        bins=20,
    )
    hist_3, bins_3 = np.histogram(
        np.multiply(np.square(0.5 * np.array(all_tot_3_4)), np.array(all_time_3_4)),
        range=[0, 1575],
        bins=20,
    )
    hist_4, bins_4 = np.histogram(
        np.multiply(np.square(0.5 * np.array(all_tot_4_4)), np.array(all_time_4_4)),
        range=[0, 1575],
        bins=20,
    )
    fig, ax = plt.subplots()
    # ax.bar(bins[:-1], hist, width=0.5*(bins[1]-bins[0]), label="")
    ax.set_xlabel("effective radius ^2 *dur")
    ax.set_ylabel("Number of tracks")
    ax.set_title("Effective area * track duration \n MT aperture(t)>3.5m")
    # ax.legend()
    # ax = plt.subplot(111)
    bins_0 = bins_salt
    w = 0.15 * (bins_0[1] - bins_0[0])
    colors = ["r", "b", "lightskyblue", "lightgreen", "g"]
    labels = ["SALT", "MT 1 at 4m", "MT 2 at 4m", "MT 3 at 4m", "MT 4 at 4m"]
    ax.bar(
        bins_0[:-1] - 2 * w,
        hist_salt,
        width=w,
        color=colors[0],
        align='center',
        linewidth=0,
    )
    ax.bar(
        bins_0[:-1] + w, hist_1, width=w, color=colors[1], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + 2 * w,
        hist_2,
        width=w,
        color=colors[2],
        align='center',
        linewidth=0,
    )
    ax.bar(
        bins_0[:-1] - w, hist_3, width=w, color=colors[3], align='center', linewidth=0
    )
    ax.bar(bins_0[:-1], hist_4, width=w, color=colors[4], align='center', linewidth=0)
    ax.autoscale(tight=True)
    # ax.set_xlim(3,9.6)
    ax.set_ylim(0, max(hist_4) + 100)
    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
    leg = ax.legend(
        lines,
        labels,
        fancybox=True,
        framealpha=0.5,
        # bbox_to_anchor=(1.05, 1), loc='left',
        borderaxespad=0.0,
        fontsize='xx-small',
    )


"effd^2*dur incl 20min 2m (MT >3.5 )"
if write == True:
    # new_list_eff_2
    hist_salt, bins_salt = np.histogram(
        np.multiply(np.square(0.5 * np.array(all_tot_salt)), np.array(all_time_salt)),
        range=[0, 1575],
        bins=20,
    )
    hist_1, bins_1 = np.histogram(
        np.multiply(np.square(0.5 * np.array(all_tot_1)), np.array(all_time_1)),
        range=[0, 1575],
        bins=20,
    )
    hist_2, bins_2 = np.histogram(
        np.multiply(np.square(0.5 * np.array(all_tot_2)), np.array(all_time_2)),
        range=[0, 1575],
        bins=20,
    )
    hist_3, bins_3 = np.histogram(
        np.multiply(np.square(0.5 * np.array(all_tot_3)), np.array(all_time_3)),
        range=[0, 1575],
        bins=20,
    )
    hist_4, bins_4 = np.histogram(
        np.multiply(np.square(0.5 * np.array(all_tot_4)), np.array(all_time_4)),
        range=[0, 1575],
        bins=20,
    )
    fig, ax = plt.subplots()
    # ax.bar(bins[:-1], hist, width=0.5*(bins[1]-bins[0]), label="")
    ax.set_xlabel("effective radius ^2 *dur")
    ax.set_ylabel("Number of tracks")
    ax.set_title("Effective area * track duration \n MT aperture(t)>3.5m")
    # ax.legend()
    # ax = plt.subplot(111)
    bins_0 = bins_salt
    w = 0.15 * (bins_0[1] - bins_0[0])
    colors = ["r", "b", "lightskyblue", "lightgreen", "g"]
    labels = ["SALT", "MT 1 at 2m", "MT 2 at 2m", "MT 3 at 2m", "MT 4 at 2m"]
    ax.bar(
        bins_0[:-1] - 2 * w,
        hist_salt,
        width=w,
        color=colors[0],
        align='center',
        linewidth=0,
    )
    ax.bar(
        bins_0[:-1] + w, hist_1, width=w, color=colors[1], align='center', linewidth=0
    )
    ax.bar(
        bins_0[:-1] + 2 * w,
        hist_2,
        width=w,
        color=colors[2],
        align='center',
        linewidth=0,
    )
    ax.bar(
        bins_0[:-1] - w, hist_3, width=w, color=colors[3], align='center', linewidth=0
    )
    ax.bar(bins_0[:-1], hist_4, width=w, color=colors[4], align='center', linewidth=0)
    ax.autoscale(tight=True)
    # ax.set_xlim(3,9.6)
    ax.set_ylim(0, max(hist_4) + 10)
    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
    leg = ax.legend(
        lines,
        labels,
        fancybox=True,
        framealpha=0.5,
        # bbox_to_anchor=(1.05, 1), loc='left',
        borderaxespad=0.0,
        fontsize='xx-small',
    )

write = False
"plot tracks and eff_d(t)"
if write == True:
    """test."""
    fig, (ax1, ax2) = plt.subplots(2, 1)
    dist = 4
    colors_MT = ["r", "b", "lightskyblue", "lightgreen", "g", "black"]
    labels = [
        "SALT",
        "MT 1 at {}m".format(dist),
        "MT 2 at {}m".format(dist),
        "MT 3 at {}m".format(dist),
        "MT 4 at {}m".format(dist),
        "t=0",
    ]
    maximum_x_y = 1.5
    x_linspace = np.linspace(-maximum_x_y, maximum_x_y, 100)
    y_linspace = np.zeros((100,))
    time_linspace = np.linspace(0, 0.0416667, 100)  # one hr in days
    x_track, y_track = (
        x_track_dict[2]['1649'][
            ::5,
        ],
        y_track_dict[2]['1649'][
            ::5,
        ],
    )
    time_track = date_track_dict[2]['1649'][
        ::5,
    ]
    time_track = time_track - time_track[0]
    x_linspace, y_linspace, time_linspace = x_track, y_track, time_track

    time_linspace = time_linspace * 1440

    mt1xy, mt2xy, mt3xy, mt4xy = get_MT_coords(
        x_linspace, y_linspace, dist_from_salt=dist
    )
    effdt_salt = calc_eff_d_t(x_linspace, y_linspace)
    effdt1 = calc_eff_d_t(mt1xy[0], mt1xy[1], tracker="MT")
    effdt2 = calc_eff_d_t(mt2xy[0], mt2xy[1], tracker="MT")
    effdt3 = calc_eff_d_t(mt3xy[0], mt3xy[1], tracker="MT")
    effdt4 = calc_eff_d_t(mt4xy[0], mt4xy[1], tracker="MT")
    # plt.scatter(time_linspace,effdt_salt,s=0.1)
    # plt.scatter(time_linspace, effdt1, s=0.1)
    effdtot_salt = tot_eff_d(effdt_salt, time_linspace)
    effdtot1 = tot_eff_d(effdt1, time_linspace)
    effdtot2 = tot_eff_d(effdt2, time_linspace)
    effdtot3 = tot_eff_d(effdt3, time_linspace)
    effdtot4 = tot_eff_d(effdt4, time_linspace)
    # print(effdtot_salt,effdtot1,effdtot2 ,effdtot3)
    ax2.grid(linestyle='--')
    ax2.scatter(x_linspace, y_linspace, s=0.1, color=colors_MT[0], label=labels[0])
    ax2.scatter(mt1xy[0], mt1xy[1], s=0.1, color=colors_MT[1], label=labels[1])
    ax2.scatter(mt2xy[0], mt2xy[1], s=0.1, color=colors_MT[2], label=labels[2])
    ax2.scatter(mt3xy[0], mt3xy[1], s=0.1, color=colors_MT[3], label=labels[3])
    ax2.scatter(mt4xy[0], mt4xy[1], s=0.1, color=colors_MT[4], label=labels[4])
    ax2.scatter(
        [x_linspace[0], mt1xy[0][0], mt2xy[0][0], mt3xy[0][0], mt4xy[0][0]],
        [y_linspace[0], mt1xy[1][0], mt2xy[1][0], mt3xy[1][0], mt4xy[1][0]],
        color=colors_MT[5],
        label=labels[5],
        s=0.2,
    )
    ax2.set_xlabel("x (m)")
    ax2.set_ylabel("y (m)")
    ax2.set_xlim(-5, 5)
    ax2.set_ylim(-5, 5)
    ax2.set_aspect(1.0 / ax2.get_data_ratio())
    ax1.scatter(time_linspace, effdt_salt, s=0.1, color=colors_MT[0], label=labels[0])
    ax1.scatter(time_linspace, effdt1, s=0.1, color=colors_MT[1], label=labels[1])
    ax1.scatter(time_linspace, effdt2, s=0.1, color=colors_MT[2], label=labels[2])
    ax1.scatter(time_linspace, effdt3, s=0.1, color=colors_MT[3], label=labels[3])
    ax1.scatter(time_linspace, effdt4, s=0.1, color=colors_MT[4], label=labels[4])
    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors_MT]
    # labels = ["eff D <7.75m","eff D >7.75m, <8.5m","eff D >8.5m"]
    # plt.legend(lines, labels)

    list_areas = [effdtot_salt, effdtot1, effdtot2, effdtot3, effdtot4]
    durs = np.diff(time_linspace)
    for i in range(5):
        z = list_areas[i]
        # eff_area=sum(np.multiply(z[:-1]*hexagon.area/100, durs))/sum(durs)
        # eff_d=2*(np.sqrt(1/math.pi*eff_area))
        # print( eff_area, eff_d)
        # print(eff_d)
        labels[i] += ", tot eff d={:01.1f}m".format(z)

    leg = ax2.legend(
        lines,
        labels,
        fancybox=True,
        framealpha=0.5,
        bbox_to_anchor=(1.05, 1),
        loc='upper left',
        borderaxespad=0.0,
        fontsize='xx-small',
    )
    ax1.set_title(
        """Effective diameter for a real track \n using MT aperture(t)>3.5m"""
    )
    ax1.set_xlabel("Time (min)")
    ax1.set_ylabel("Effective diameter (m)")
    plt.subplots_adjust(
        top=0.91, bottom=0.07, left=0.079, right=0.978, hspace=0.255, wspace=0.2
    )

"plt frac tracks >20min >3.5m"
fig, ax = plt.subplots()
ax.scatter(
    [2, 2.5, 3, 3.5, 3.75, 4],
    [two[0], two5[0], three[0], three5[0], three75[0], four[0]],
    color=colors[1],
)
ax.scatter(
    [2, 2.5, 3, 3.5, 3.75, 4],
    [two[1], two5[1], three[1], three5[1], three75[1], four[1]],
    color=colors[2],
)
ax.scatter(
    [2, 2.5, 3, 3.5, 3.75, 4],
    [two[2], two5[2], three[2], three5[2], three75[2], four[2]],
    color=colors[3],
)
ax.scatter(
    [2, 2.5, 3, 3.5, 3.75, 4],
    [two[3], two5[3], three[3], three5[3], three75[3], four[3]],
    color=colors[4],
)
colors = ['r', 'b', 'lightskyblue', 'lightgreen', 'g', "black"]
ax.plot(
    [2, 2.5, 3, 3.5, 3.75, 4],
    [
        np.average(two),
        np.average(two5),
        np.average(three),
        np.average(three5),
        np.average(three75),
        np.average(four),
    ],
    color="black",
    ls="--",
)
lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors[1:]]
labels = ["MT 1", "MT 2", "MT 3", "MT 4", "Average"]
ax.set_title(
    "Percentage of SALT tracks with usable MT tracks \n MT diam(t)>3.5m, MT track >20min"
)
ax.set_xlabel("Distance from SALT tracker (m)")
ax.set_ylabel("% of tracks")
ax.grid()
leg = ax.legend(
    lines,
    labels,
    fancybox=True,
    framealpha=0.5,
    # bbox_to_anchor=(1.05, 1), loc='left',
    borderaxespad=0.0,
    fontsize='xx-small',
)

# effective_area=sum(np.multiply(z[:-1]*hexagon.area/100, 1440*durs))/sum(1440*durs)#sum(z)/len(z)*hexagon.area
# effective_time=sum(1440*(durs))
# dia_i=2*(np.sqrt(1/math.pi*np.array(effective_area)))
# effective_area_1=sum(np.multiply(z[:-1]*hexagon.area/100, 1440*durs))/sum(1440*durs)#sum(z)/len(z)*hexagon.area
# effective_time_1=sum(1440*(durs))
# dia_i_1=2*(np.sqrt(1/math.pi*np.array(effective_area)))
# effective_area_2=sum(np.multiply(z[:-1]*hexagon.area/100, 1440*durs))/sum(1440*durs)#sum(z)/len(z)*hexagon.area
# effective_time_2=sum(1440*(durs))
# dia_i_2=2*(np.sqrt(1/math.pi*np.array(effective_area)))
# effective_area_3=sum(np.multiply(z[:-1]*hexagon.area/100, 1440*durs))/sum(1440*durs)#sum(z)/len(z)*hexagon.area
# effective_time_3=sum(1440*(durs))
# dia_i_3=2*(np.sqrt(1/math.pi*np.array(effective_area)))
# effective_area_4=sum(np.multiply(z[:-1]*hexagon.area/100, 1440*durs))/sum(1440*durs)#sum(z)/len(z)*hexagon.area
# effective_time_4=sum(1440*(durs))
# dia_i_4=2*(np.sqrt(1/math.pi*np.array(effective_area)))


# import pickle
# f = pickle.load( open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/f_new.p", "rb" ) )
# g = pickle.load( open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/g_new.p", "rb" ) )
# f_MT = pickle.load( open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/f_MT.p", "rb" ) )
# g_MT = pickle.load( open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/g_MT.p", "rb" ) )


# pickle.dump( f_MT, open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/f_MT.p", "wb" ) )
# pickle.dump( g_MT, open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/g_MT.p", "wb" ) )


# pickle.dump( f, open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/f_jun.p", "wb" ) )
# pickle.dump( f_MT, open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/f_MT_june.p", "wb" ) )
