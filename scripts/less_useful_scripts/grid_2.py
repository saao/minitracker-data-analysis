#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 09:45:15 2020

@author: freya
"""

from shapely.geometry import Point, LineString, Polygon
import math
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from matplotlib.colors import LogNorm


def get_csv():
    file="201{}_{}_14_nights.csv".format(year, split)
    csv = np.genfromtxt ('./data/new_data/sorted/write_to/{}'.format(file),
                         delimiter=",",
                         skip_header=1,dtype=float,
                         usecols=(0,1,2)
                          )
    return csv
csv_xy=get_csv()

r=5
a=2*(r)/math.sqrt(3)
b=1/math.sqrt(3)
h=0.5
#hexagon= Polygon([(0,a), (r,a/2), (r, -a/2), (0,-a), (-r, -a/2), (-r, a/2)])
a=5.5
hexagon=Polygon([(-0.5*b, a),(0.5*b, a),
                (b, a-h),(2*b, a-h),(2.5*b, a-2*h),(3.5*b, a-2*h),
                (4*b,a-3*h),(5*b, a-3*h),(5.5*b, a-4*h),(6.5*b, a-4*h),
                (7*b,a-5*h),(8*b, a-5*h),
                (8.5*b, a-6*h), #corner
                (8*b, a-7*h),(8.5*b, a-8*h),(8*b, a-9*h),(8.5*b, a-10*h),
                (8*b, a-11*h),(8.5*b, a-12*h),(8*b, a-13*h),
                (8.5*b, a-14*h),(8*b, a-15*h),(8.5*b, a-16*h),
                (8*b, a-17*h), #corner
                (7*b,a-17*h),(6.5*b, a-18*h),(5.5*b, a-18*h),(5*b, a-19*h),
                (4*b, a-19*h), (3.5*b, a-20*h),(2.5*b, a-20*h),
                (2*b, a-21*h),(1*b, a-21*h),(0.5*b,a-22*h),
                (-0.5*b,a-22*h),#corner
                (-b, a-21*h),(-2*b, a-21*h),(-2.5*b, a-20*h),(-3.5*b, a-20*h),
                (-4*b,a-19*h),(-5*b, a-19*h),(-5.5*b, a-18*h),(-6.5*b, a-18*h),
                (-7*b,a-17*h),(-8*b, a-17*h),
                (-8.5*b, a-16*h),#corner
                (-8*b, a-15*h),(-8.5*b, a-14*h),(-8*b, a-13*h),(-8.5*b, a-12*h),
                (-8*b, a-11*h),(-8.5*b, a-10*h),(-8*b, a-9*h),
                (-8.5*b, a-8*h),(-8*b, a-7*h),(-8.5*b, a-6*h),
                (-8*b, a-5*h), #corner
                (-7*b,a-5*h),(-6.5*b, a-4*h),(-5.5*b, a-4*h),(-5*b, a-3*h),
                (-4*b, a-3*h), (-3.5*b, a-2*h),(-2.5*b, a-2*h),
                (-2*b, a-1*h),(-1*b, a-1*h)
                ])
#xh,yh=hexhex.exterior.xy
#x,y,=hexagon.exterior.xy
#fig,ax=plt.subplots(1,1)
#ax.set_aspect(1./ax.get_data_ratio())
#ax.plot(xh,yh)
#ax.plot(x,y)
#ax.plot([-6,0,6],[0,0,0])
#ax.plot([0,0,0],[-6,0,6])


"////"
covered=[]
def get_coverage_x_y(x_y):
    coverage=[]
    for row in x_y:
        xp=(26/13) * row[0]
        yp=(26/13)*row[1]
        centre_annulus=Point(xp,yp)
        "////"
        outer_circle=centre_annulus.buffer(5.5)
        inner_circle=centre_annulus.buffer(3.5/2)
        annulus=outer_circle.difference(inner_circle)
        mirror_covered=hexagon.intersection(annulus)
        percentage_mirror=100*mirror_covered.area/hexagon.area
#        print(percentage_mirror)
        coverage.append(percentage_mirror)
    return coverage

def get_coverage(x,y):
#    coverage=[]
#    for i in range(len(x)):
    xp=(26/13) *x
    yp=(26/13)*y
    centre_annulus=Point(xp,yp)
    "////"
    outer_circle=centre_annulus.buffer(5.5)
    inner_circle=centre_annulus.buffer(3.5/2)
    annulus=outer_circle.difference(inner_circle)
    mirror_covered=hexagon.intersection(annulus)
    percentage_mirror=100*mirror_covered.area/hexagon.area
#        print(percentage_mirror)
#    coverage.append(percentage_mirror)
    return percentage_mirror
    
    

#
#        
#cmap = sns.cubehelix_palette(as_cmap=True) #colour scheme
#x_range=np.arange(0,2.5, 0.1)
#y_range=np.arange(0,2.5,0.1)
#x_y=[]
#for i in range(len(x_range)):
#    for k in range(len(y_range)):
#        x_y.append((x_range[i], y_range[k]))
#
#fig = plt.figure() 
#ax = fig.add_subplot(1,1,1)
#
#fig, ax = plt.subplots()
#ax.set_aspect(1./ax.get_data_ratio())
#coverage=get_coverage_x_y(x_y)
#x_y_np=np.asarray(x_y)
#plot=ax.scatter(x_y_np[:,0], x_y_np[:,1], c=coverage, cmap=cmap)
#fig.colorbar(plot)




at=0.5*a
rt=0.5*r


xlist = np.linspace(-3, 3, 100)
ylist = np.linspace(-3, 3, 100)
X, Y = np.meshgrid(xlist, ylist)
Z = np.vectorize(get_coverage)(X,Y)
fig,ax=plt.subplots(1,1)
ax.set_aspect(1./ax.get_data_ratio())
cp = ax.contour(X,Y, Z)
ax.clabel(cp, inline=True, inline_spacing=3, rightside_up=True)#, colors='k', fontsize=8, fmt=fmt)
ax.set_xlim(-3,3)
ax.set_ylim(-3,3)

#for k in range(len(break_up)+1):
#        count=0
#        for i in x_track_dict[k]:
#    #        print(type(i), i)
#            if count==0:
#                plot=ax.plot(x_track_dict[k][i], y_track_dict[k][i],
#                    color=colors[k],alpha=0.5, label=times[k])
#            else:
#                plot=ax.plot(x_track_dict[k][i], y_track_dict[k][i],
#                    color=colors[k],alpha=(0.5/(k+1)+0.4))
#            count+=1
#ax.legend()
ax.set_title('Percentage of mirror covered by pupil for tracker x-y')
ax.set_xlabel('x (m)')
ax.set_ylabel('y (m)')
def hext(a,b,h):
    a=0.5*a
    b=0.5*b
    h=0.5*h
    hexagon=Polygon([(-0.5*b, a),(0.5*b, a),
                    (b, a-h),(2*b, a-h),(2.5*b, a-2*h),(3.5*b, a-2*h),
                    (4*b,a-3*h),(5*b, a-3*h),(5.5*b, a-4*h),(6.5*b, a-4*h),
                    (7*b,a-5*h),(8*b, a-5*h),
                    (8.5*b, a-6*h), #corner
                    (8*b, a-7*h),(8.5*b, a-8*h),(8*b, a-9*h),(8.5*b, a-10*h),
                    (8*b, a-11*h),(8.5*b, a-12*h),(8*b, a-13*h),
                    (8.5*b, a-14*h),(8*b, a-15*h),(8.5*b, a-16*h),
                    (8*b, a-17*h), #corner
                    (7*b,a-17*h),(6.5*b, a-18*h),(5.5*b, a-18*h),(5*b, a-19*h),
                    (4*b, a-19*h), (3.5*b, a-20*h),(2.5*b, a-20*h),
                    (2*b, a-21*h),(1*b, a-21*h),(0.5*b,a-22*h),
                    (-0.5*b,a-22*h),#corner
                    (-b, a-21*h),(-2*b, a-21*h),(-2.5*b, a-20*h),(-3.5*b, a-20*h),
                    (-4*b,a-19*h),(-5*b, a-19*h),(-5.5*b, a-18*h),(-6.5*b, a-18*h),
                    (-7*b,a-17*h),(-8*b, a-17*h),
                    (-8.5*b, a-16*h),#corner
                    (-8*b, a-15*h),(-8.5*b, a-14*h),(-8*b, a-13*h),(-8.5*b, a-12*h),
                    (-8*b, a-11*h),(-8.5*b, a-10*h),(-8*b, a-9*h),
                    (-8.5*b, a-8*h),(-8*b, a-7*h),(-8.5*b, a-6*h),
                    (-8*b, a-5*h), #corner
                    (-7*b,a-5*h),(-6.5*b, a-4*h),(-5.5*b, a-4*h),(-5*b, a-3*h),
                    (-4*b, a-3*h), (-3.5*b, a-2*h),(-2.5*b, a-2*h),
                    (-2*b, a-1*h),(-1*b, a-1*h)
                    ])
    return hexagon
hext=hext(a,b,h)
xt,yt=hext.exterior.xy
ax.plot(xt,yt)

plt.show()