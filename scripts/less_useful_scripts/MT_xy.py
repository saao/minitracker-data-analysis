#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 15 10:22:00 2020

@author: freya
"""
from shapely.geometry import Point, LineString, Polygon
import math
import matplotlib.pyplot as plt

tracker_xy=(1,1)
tracks=[(0,3),
        (0.3, -2.5),
        (-1,2),
        ((-0.9,1.7))]

# tracker_point=Point(tracker_xy[0], tracker_xy[1])
len_sides=1.6/2
r=[0.2,2]
theta=[60,210]
MT_centers=[Point(tracker_xy[0]+len_sides, tracker_xy[1]+len_sides),
            Point(tracker_xy[0]+len_sides, tracker_xy[1]-len_sides),
            Point(tracker_xy[0]-len_sides, tracker_xy[1]-len_sides),
            Point(tracker_xy[0]-len_sides, tracker_xy[1]+len_sides)]
def sector(center, start_angle, end_angle, radius=r[1], steps=200, inner_rad=r[0]):
    def polar_point(origin_point, angle,  distance):
        return [origin_point.x + math.sin(math.radians(angle)) * distance, origin_point.y + math.cos(math.radians(angle)) * distance]

    if start_angle > end_angle:
        start_angle = start_angle - 360
    else:
        pass
    step_angle_width = (end_angle-start_angle) / steps
    sector_width = (end_angle-start_angle) 
    segment_vertices = []

    segment_vertices.append(polar_point(center, 0,0))
    segment_vertices.append(polar_point(center, start_angle,radius))
    for z in range(1, steps):
        segment_vertices.append((polar_point(center, start_angle + z * step_angle_width,radius)))
    segment_vertices.append(polar_point(center, start_angle+sector_width,radius))
    segment_vertices.append(polar_point(center, 0,0))
    circle= Polygon(segment_vertices)
    smaller_circle=center.buffer(inner_rad)
    annulus=circle.difference(smaller_circle)
    return annulus
angles=(60,210)
sectors=[sector(MT_centers[0], angles[0]-90,angles[1]-90),
         sector(MT_centers[1],-angles[1]-90,-angles[0]-90 ),
         sector(MT_centers[2], angles[0]+90,angles[1]+90),
         sector(MT_centers[3],-angles[1]+90,-angles[0]+90 )
         ]
fig, ax = plt.subplots()
ax.set_aspect(1./ax.get_data_ratio())
ax.scatter(tracker_xy[0], tracker_xy[1])
for sector in sectors:
    x,y=sector.exterior.xy
    ax.plot(x,y)