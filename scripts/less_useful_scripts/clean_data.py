#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 14:14:45 2020

@author: freya
"""

import numpy as np
import julian
from datetime import datetime
import pytz
import math
import matplotlib.pyplot as plt
import seaborn as sns

def convert_date(d_bytes):
    s = d_bytes.decode('utf-8')
    tzone=pytz.timezone('Africa/Johannesburg')
    try:
        dt=datetime.strptime(s, "%Y-%m-%d %H:%M:%S.%f")
    except:
        dt=datetime.strptime(s, "%Y-%m-%d %H:%M:%S")
    dt_local=tzone.localize(dt)
    mjd=julian.to_jd(dt_local, fmt="mjd")
    return mjd

def get_csv():
    csv = np.genfromtxt ('/home/freya/programming/MiniTrackers/danny/new/data/pointing_2014.csv',
                         delimiter=",",
                         skip_header=1,dtype=float,
                         converters={0: convert_date},
                         usecols=(0,1,2,3,4)
                          )
    return csv


def split_csv(csv):
    timestamp = csv[:,0]
    dec = csv[:,1]
    ra = csv[:,3]
    track_paused = csv[:,4]
    return timestamp,ra,dec,track_paused

def get_decimal(mjd):
    frac, int_as_float=math.modf(mjd)
    return frac

def delete_day(csv):
    csv=csv[np.logical_not
            (np.logical_and(
                    np.vectorize(get_decimal)(csv[:,0])>0.3725,
                    np.vectorize(get_decimal)(csv[:,0])<0.625))]
    return csv

def delete_tracks_paused(csv):
    csv=csv[np.logical_not(csv[:,4]!=0)]
    return csv
def delete_guides_paused(csv):
    csv=csv[np.logical_not(csv[:,2]!=0)]
    return csv

def rad_to_deg(array):
    degrees=np.degrees(array)
    return degrees
    
csv=get_csv()
csv_nights=delete_day(csv)
csv_tracking=delete_tracks_paused(csv_nights)
csv_guiding=delete_guides_paused(csv_tracking)
csv_use=csv_guiding
timestamp,ra,dec,track_paused=split_csv(csv_use)
ra_deg=rad_to_deg(ra)
dec_deg=rad_to_deg(dec)
time_of_day=np.vectorize(get_decimal)(timestamp)

#cmap = sns.cubehelix_palette(as_cmap=True, reverse=True) #colour scheme
#plt.scatter(timestamp,dec_deg, s=0.1,c=ra_deg, cmap=cmap)

fig, ax1=plt.subplots()
ax2 = ax1.twinx()
ax1.scatter(timestamp,dec_deg, s=1,color='skyblue')#, alpha=0.75)
ax2.scatter(timestamp,ra_deg,s=1, color='y')#, alpha=0.75)
plt.show()


time_stopped_i=0
track_list=[]
discarded=0
ra_new=[]
dec_new=[]
time_new=[]
for i in range(len(csv_use)-1):
    time_now=timestamp[i]
#    print("time i",time_now)
    if abs(ra_deg[i+1]-ra_deg[i])<0.2 and abs(dec_deg[i+1]-dec_deg[i])<0.5:
#        print("tracking")
        if abs(ra_deg[i-1]-ra_deg[i])<0.2 and abs(ra_deg[i-1]-ra_deg[i-2])>0.2:
            start=timestamp[i]
            ra_new.append(ra_deg[i])
            dec_new.append(dec_deg[i])
            time_new.append(timestamp[i])
            still_tracking=True
#        time_still_tracking=time_now
#        time_tracked=time_still_tracking-time_stopped
    elif  abs(ra_deg[i+1]-ra_deg[i])>0.2 and abs(ra_deg[i-1]-ra_deg[i])<0.2:
        end=timestamp[i]
        start=timestamp[i]
        ra_new.append(ra_deg[i-1])
        dec_new.append(dec_deg[i-1])
        time_new.append(timestamp[i-1])
#        previous_stop_i=time_stopped_i+1
#        time_stopped_i=i
#        print("previous stop",previous_stop)
#        print("this_stop", time_stopped)
        time_elapsed=end-start
        if time_elapsed<0.291667: #10s and 7 hrs 0.000115741<
            discarded+=1
            track_list.append(time_elapsed)

#track_lengths=[]        
#for track in track_list:
#    track_length=track[1]-track[0]
#    track_lengths.append(track_length)
fig, ax1=plt.subplots()
ax2 = ax1.twinx()
#ax1.scatter(time_new,dec_new, s=1,color='r')#, alpha=0.75)
ax2.scatter(time_new,ra_new,s=1, color='b')#, alpha=0.75)
#plt.plot(time_new,ra_new)
plt.show()

durations=np.array(track_list)
fig, ax=plt.subplots()
durations_seconds=86400*durations
sorted_durations=np.sort(durations_seconds)
hist_durations, bins=np.histogram(sorted_durations,
                                  bins=1000, range=[0,0.291667])
#plt.hist(hist_durations[1:], bins=1000)#, bins=1000,range=[0,0.5])
ax.bar(bins[:-1], hist_durations, 0.25 )
ax.set_xlim(0.000115741,0.291667)
plt.show()