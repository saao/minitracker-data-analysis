#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr  3 09:45:15 2020

@author: freya
"""

from shapely.geometry import Point,  Polygon
import math
import matplotlib.pyplot as plt
import numpy as np
# import seaborn as sns
# from matplotlib.colors import LogNorm
from scipy import interpolate


# def get_csv():
#     file="2013_2_14_nights.csv"#"201{}_{}_14_nights.csv".format(year, split)
#     csv = np.genfromtxt ('./data/new_data/sorted/write_to/{}'.format(file),
#                          delimiter=",",
#                          skip_header=1,dtype=float,
#                          usecols=(0,1,2)
#                           )
#     return csv
# csv_xy=get_csv()

"define the mirror"
r=5.0000000
a=2*(r)/math.sqrt(3.0000000)
b=1.0000000/math.sqrt(3.0000000)
h=0.50000000
#hexagon= Polygon([(0,a), (r,a/2), (r, -a/2), (0,-a), (-r, -a/2), (-r, a/2)])
a=5.50000000
hexagon=Polygon([(-0.5*b, a),(0.5*b, a),
                (b, a-h),(2*b, a-h),(2.5*b, a-2*h),(3.5*b, a-2*h),
                (4*b,a-3*h),(5*b, a-3*h),(5.5*b, a-4*h),(6.5*b, a-4*h),
                (7*b,a-5*h),(8*b, a-5*h),
                (8.5*b, a-6*h), #corner
                (8*b, a-7*h),(8.5*b, a-8*h),(8*b, a-9*h),(8.5*b, a-10*h),
                (8*b, a-11*h),(8.5*b, a-12*h),(8*b, a-13*h),
                (8.5*b, a-14*h),(8*b, a-15*h),(8.5*b, a-16*h),
                (8*b, a-17*h), #corner
                (7*b,a-17*h),(6.5*b, a-18*h),(5.5*b, a-18*h),(5*b, a-19*h),
                (4*b, a-19*h), (3.5*b, a-20*h),(2.5*b, a-20*h),
                (2*b, a-21*h),(1*b, a-21*h),(0.5*b,a-22*h),
                (-0.5*b,a-22*h),#corner
                (-b, a-21*h),(-2*b, a-21*h),(-2.5*b, a-20*h),(-3.5*b, a-20*h),
                (-4*b,a-19*h),(-5*b, a-19*h),(-5.5*b, a-18*h),(-6.5*b, a-18*h),
                (-7*b,a-17*h),(-8*b, a-17*h),
                (-8.5*b, a-16*h),#corner
                (-8*b, a-15*h),(-8.5*b, a-14*h),(-8*b, a-13*h),(-8.5*b, a-12*h),
                (-8*b, a-11*h),(-8.5*b, a-10*h),(-8*b, a-9*h),
                (-8.5*b, a-8*h),(-8*b, a-7*h),(-8.5*b, a-6*h),
                (-8*b, a-5*h), #corner
                (-7*b,a-5*h),(-6.5*b, a-4*h),(-5.5*b, a-4*h),(-5*b, a-3*h),
                (-4*b, a-3*h), (-3.5*b, a-2*h),(-2.5*b, a-2*h),
                (-2*b, a-1*h),(-1*b, a-1*h)
                ])

#xh,yh=hexhex.exterior.xy
#x,y,=hexagon.exterior.xy
#fig,ax=plt.subplots(1,1)
#ax.set_aspect(1./ax.get_data_ratio())
#ax.plot(xh,yh)
#ax.plot(x,y)
#ax.plot([-6,0,6],[0,0,0])
#ax.plot([0,0,0],[-6,0,6])


"////"
covered=[]
# def get_coverage_x_y(x_y):
#     coverage=[]
#     for row in x_y:
#         xp=(26/13) * row[0]
#         yp=(26/13)*row[1]
#         centre_annulus=Point(xp,yp)
#         "////"
#         outer_circle=centre_annulus.buffer(5.5)
#         inner_circle=centre_annulus.buffer(3.5/2)
#         annulus=outer_circle.difference(inner_circle)
#         mirror_covered=hexagon.intersection(annulus)
#         percentage_mirror=100*mirror_covered.area/hexagon.area
# #        print(percentage_mirror)
#         coverage.append(percentage_mirror)
#     return coverage

"get percentage of mirror covered by MT annulus centered at any given xy"
def get_coverage_MT(x,y):
#    coverage=[]
#    for i in range(len(x)):
    xp=(26/13) *x
    yp=(26/13)*y
    centre_annulus=Point(xp,yp)
    "////"
    outer_circle=centre_annulus.buffer(4.5)
    inner_circle=centre_annulus.buffer(1)
    annulus=outer_circle.difference(inner_circle)
    mirror_covered=hexagon.intersection(annulus)
    percentage_mirror=100*mirror_covered.area/hexagon.area
#        print(percentage_mirror)
#    coverage.append(percentage_mirror)
    return percentage_mirror

"get percentage of mirror covered by annulus centered at any given xy"    
def get_coverage(x,y):
#    coverage=[]
#    for i in range(len(x)):
    xp=(26/13) *x
    yp=(26/13)*y
    centre_annulus=Point(xp,yp)
    "////"
    outer_circle=centre_annulus.buffer(5.5)
    inner_circle=centre_annulus.buffer(3.5/2)
    annulus=outer_circle.difference(inner_circle)
    mirror_covered=hexagon.intersection(annulus)
    percentage_mirror=100*mirror_covered.area/hexagon.area
#        print(percentage_mirror)
#    coverage.append(percentage_mirror)
    return percentage_mirror


        
# cmap = sns.cubehelix_palette(as_cmap=True) #colour scheme
# x_range=np.arange(0,2.5, 0.1)
# y_range=np.arange(0,2.5,0.1)
# x_y=[]
# for i in range(len(x_range)):
#     for k in range(len(y_range)):
#         x_y.append((x_range[i], y_range[k]))

# fig = plt.figure() 
# ax = fig.add_subplot(1,1,1)

# fig, ax = plt.subplots()
# ax.set_aspect(1./ax.get_data_ratio())
# coverage=get_coverage_x_y(x_y)
# x_y_np=np.asarray(x_y)
# plot=ax.scatter(x_y_np[:,0], x_y_np[:,1], c=coverage, cmap=cmap)
# fig.colorbar(plot)




# at=0.5*a
# rt=0.5*r


# xlist = np.linspace(-3, 3, 50)
# ylist = np.linspace(-3, 3, 50)
# X, Y = np.meshgrid(xlist, ylist)
# Z = np.vectorize(get_coverage)(X,Y)
# fig,ax=plt.subplots(1,1)
# ax.set_aspect(1./ax.get_data_ratio())
# cp = ax.contour(X,Y, Z)
# ax.clabel(cp, inline=True, inline_spacing=3, rightside_up=True)#, colors='k', fontsize=8, fmt=fmt)
# ax.set_xlim(-3,3)
# ax.set_ylim(-3,3)
# plt.show()
# ax.set_title('Percentage of mirror covered by pupil for tracker x-y')
# ax.set_xlabel('x (m)')
# ax.set_ylabel('y (m)')
# ax.grid(True, linestyle="--", alpha=0.1, color='grey')
# plt.show()

def hext(a,b,h):
    a=0.5*a
    b=0.5*b
    h=0.5*h
    hexagon=Polygon([(-0.5*b, a),(0.5*b, a),
                    (b, a-h),(2*b, a-h),(2.5*b, a-2*h),(3.5*b, a-2*h),
                    (4*b,a-3*h),(5*b, a-3*h),(5.5*b, a-4*h),(6.5*b, a-4*h),
                    (7*b,a-5*h),(8*b, a-5*h),
                    (8.5*b, a-6*h), #corner
                    (8*b, a-7*h),(8.5*b, a-8*h),(8*b, a-9*h),(8.5*b, a-10*h),
                    (8*b, a-11*h),(8.5*b, a-12*h),(8*b, a-13*h),
                    (8.5*b, a-14*h),(8*b, a-15*h),(8.5*b, a-16*h),
                    (8*b, a-17*h), #corner
                    (7*b,a-17*h),(6.5*b, a-18*h),(5.5*b, a-18*h),(5*b, a-19*h),
                    (4*b, a-19*h), (3.5*b, a-20*h),(2.5*b, a-20*h),
                    (2*b, a-21*h),(1*b, a-21*h),(0.5*b,a-22*h),
                    (-0.5*b,a-22*h),#corner
                    (-b, a-21*h),(-2*b, a-21*h),(-2.5*b, a-20*h),(-3.5*b, a-20*h),
                    (-4*b,a-19*h),(-5*b, a-19*h),(-5.5*b, a-18*h),(-6.5*b, a-18*h),
                    (-7*b,a-17*h),(-8*b, a-17*h),
                    (-8.5*b, a-16*h),#corner
                    (-8*b, a-15*h),(-8.5*b, a-14*h),(-8*b, a-13*h),(-8.5*b, a-12*h),
                    (-8*b, a-11*h),(-8.5*b, a-10*h),(-8*b, a-9*h),
                    (-8.5*b, a-8*h),(-8*b, a-7*h),(-8.5*b, a-6*h),
                    (-8*b, a-5*h), #corner
                    (-7*b,a-5*h),(-6.5*b, a-4*h),(-5.5*b, a-4*h),(-5*b, a-3*h),
                    (-4*b, a-3*h), (-3.5*b, a-2*h),(-2.5*b, a-2*h),
                    (-2*b, a-1*h),(-1*b, a-1*h)
                    ])
    return hexagon
#hext=hext(a,b,h)
#xt,yt=hext.exterior.xy
#ax.plot(xt,yt)

# xlist = np.linspace(-3, 3, 50)
# ylist = np.linspace(-3, 3, 50)
# X, Y = np.meshgrid(xlist, ylist)
# Z = np.vectorize(get_coverage_MT)(X,Y)
# fig,ax=plt.subplots(1,1)
# ax.set_aspect(1./ax.get_data_ratio())
# cp = ax.contour(X,Y, Z)
# ax.clabel(cp, inline=True, inline_spacing=3, rightside_up=True)#, colors='k', fontsize=8, fmt=fmt)
# ax.set_xlim(-3,3)
# ax.set_ylim(-3,3)
# plt.show()
# ax.set_title('Percentage of mirror covered by pupil for MiniTracker x-y')
# ax.set_xlabel('x (m)')
# ax.set_ylabel('y (m)')
# ax.grid(True, linestyle="--", alpha=0.1, color='grey')
# plt.show()


"SLOW PART"
# x = np.arange(-3.2, 3.2, 0.01)
# y = np.arange(-3.2, 3.2, 0.01)
# xx, yy = np.meshgrid(x, y)
# z = np.vectorize(get_coverage)(xx,yy)
# z_MT=np.vectorize(get_coverage_MT)(xx,yy)
# f = interpolate.interp2d(x, y, z, kind='cubic')
# f_MT = interpolate.interp2d(x, y, z_MT, kind='cubic')
# x_low=np.arange(-3.2, 3.2, 0.15)
# y_low=np.arange(-3.2, 3.2, 0.15)
# xxlow,yylow=np.meshgrid(x_low,y_low)
# zlow=np.vectorize(get_coverage)(xxlow,yylow)
# g=interpolate.interp2d(xxlow,yylow,zlow, kind="cubic")
# zlow_MT=np.vectorize(get_coverage_MT)(xxlow,yylow)
# g_MT=interpolate.interp2d(xxlow,yylow,zlow_MT, kind="cubic")
"END SLOW PART"

# xnew = np.arange(-3.2, 3.1, 1e-2)
# ynew = np.arange(-3.2, 3.1, 1e-2)
# znew = f(xnew, ynew)
# plt.plot(x, z[0, :], 'ro-', xnew, znew[0, :], 'b-')
# plt.show()

# fig,ax=plt.subplots(1,1)
# ax.set_aspect(1./ax.get_data_ratio())
# cp = ax.contour(xnew,ynew,znew)
# xnew = np.arange(-3.2, 3.1, 1e-2)
# ynew = np.arange(-3.2, 3.1, 1e-2)
# znew = f(xnew, ynew)
# plt.plot(x, z[0, :], 'ro-', xnew, znew[0, :], 'b-')
# plt.show()
# fig,ax=plt.subplots(1,1)
# ax.set_aspect(1./ax.get_data_ratio())
# cp = ax.contour(xnew,ynew,znew)
# plt.show()
# fig,ax=plt.subplots(1,1)
# ax.set_aspect(1./ax.get_data_ratio())
# cp = ax.contour(x,y,z)

def lookup_coverage(x,y, res="high"):
    if res=="low":
        z=g(x,y)
    else:
        z=f(x,y)
    return z[:,0]

def lookup_coverage_MT(x,y, res="high"):
    if res=="low":
        z=g_MT(x,y)
    else:
        z=f_MT(x,y)
    return z[:,0]

zinterp=lookup_coverage(csv_use[::100,1],csv_use[::100,2], res="low")
plt.scatter(csv_use[::100,1], zinterp, s=0.2)
plt.show()






diameters_list=[]
mirror_area={}
duration={}
z_list=[]
dur_list=[]
#fig, ax = plt.subplots()
for k in [1,2]:#range(len(break_up)+1):
        count=0
        mirror_area[k]={}
        duration[k]={}
        for i in x_track_dict[k]:
            times=date_track_dict[k][i][::10]
            durs=np.diff(times)
            z=lookup_coverage(x_track_dict[k][i][::10],y_track_dict[k][i][::10],
                              res="high")
            effective_area=sum(np.multiply(z[:-1]*hexagon.area/100, 1440*durs))/sum(1440*durs)#sum(z)/len(z)*hexagon.area
            effective_time=sum(1440*(durs))
#            print(effective_area, effective_time)
            mirror_area[k][i]=z
            duration[k][i]=durs
            dur_list.extend(list(durs))
            z_list.append(effective_area)
            
#            ax.scatter(z[:-1]*hexagon.area, 1440*(durs), alpha=0.3, s=0.3)
#            ax.plot(count,effective_area)#,s=0.5)#, alpha=0.6)
            
            count+=1
diameters=2*(np.sqrt(1/math.pi*np.array(z_list)))
diameters_list.extend(diameters)
##diameters_list=[]
#ax.plot(diameters, "-o", alpha=0.3)            
#ax.set_xlabel("Observation number")
#ax.set_ylabel("Effective diameter (m)")
#ax.axhline(y=2*(np.sqrt(hexagon.area*1/math.pi)), linestyle="--", label="SALT mirror area")
#ax.set_title("Effective observing area for ~80 observations (Dec 2011)")
#ax.legend()
##
##hist, bins=np.histogram(z_list)
##plt.bar(bins[:-1], hist)




fig, ax = plt.subplots()
hist, bins=np.histogram(diameters_list,range=[6.5,10], bins=35)
ax.bar(bins[:-1], hist, width=0.5*(bins[1]-bins[0]), label="")
ax.set_xlabel("Effective diameter (m)")
ax.set_ylabel("Observations")
ax.set_title("Effective diameter per observation \nSALT 201{}".format(year))
#ax.legend()



tracker_xy=(1,1)
# tracker_point=Point(tracker_xy[0], tracker_xy[1])
len_sides=1.6/2
r=[0.2,2]
theta=[60,210]
MT_centers=[Point(tracker_xy[0]+len_sides, tracker_xy[1]+len_sides),
            Point(tracker_xy[0]+len_sides, tracker_xy[1]-len_sides),
            Point(tracker_xy[0]-len_sides, tracker_xy[1]-len_sides),
            Point(tracker_xy[0]-len_sides, tracker_xy[1]+len_sides)]
def polar_point(origin_point, angle,  distance):
    return [origin_point.x + math.sin(math.radians(angle)) * distance, origin_point.y + math.cos(math.radians(angle)) * distance]

def sector(center, start_angle, end_angle, radius=r[1], steps=200, inner_rad=r[0]):
    
    if start_angle > end_angle:
        start_angle = start_angle - 360
    else:
        pass
    step_angle_width = (end_angle-start_angle) / steps
    sector_width = (end_angle-start_angle) 
    segment_vertices = []

    segment_vertices.append(polar_point(center, 0,0))
    segment_vertices.append(polar_point(center, start_angle,radius))
    for z in range(1, steps):
        segment_vertices.append((polar_point(center, start_angle + z * step_angle_width,radius)))
    segment_vertices.append(polar_point(center, start_angle+sector_width,radius))
    segment_vertices.append(polar_point(center, 0,0))
    circle= Polygon(segment_vertices)
    smaller_circle=center.buffer(inner_rad)
    annulus=circle.difference(smaller_circle)
    return annulus
angles=(60,210)
sectors=[sector(MT_centers[0], angles[0]-90,angles[1]-90),
         sector(MT_centers[1],-angles[1]-90,-angles[0]-90 ),
         sector(MT_centers[2], angles[0]+90,angles[1]+90),
         sector(MT_centers[3],-angles[1]+90,-angles[0]+90 )
         ]
fig, ax = plt.subplots()
for sector in sectors:
    x,y=sector.exterior.xy
    ax.plot(x,y)



# import pickle
# f = pickle.load( open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/f.p", "rb" ) )
# g = pickle.load( open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/g.p", "rb" ) )


