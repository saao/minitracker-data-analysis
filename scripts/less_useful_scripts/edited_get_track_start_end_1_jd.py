
"""
Created on Wed 22 April

@author: freya
"""
# import random
import numpy as np
import julian
from datetime import datetime
import pytz
import csv as csv_writer
import math
# import matplotlib.pyplot as plt

"Changeable variables"
year=4 #2019
minimum_track=0.00694444  #10 min in days

"convert datetime to julian day float"
def convert_date(d_bytes):
    s = d_bytes.decode('utf-8')
    tzone=pytz.timezone('Africa/Johannesburg')
    try:
        dt=datetime.strptime(s, "%Y-%m-%d %H:%M:%S.%f")
    except:
        dt=datetime.strptime(s, "%Y-%m-%d %H:%M:%S")
    dt_local=tzone.localize(dt)
    mjd=julian.to_jd(dt_local, fmt="jd")
    return mjd

"convert days to minutes"
def day_to_time(days, option="min"):
    date=julian.from_jd(days, fmt="jd")
    seconds=86400*days
    mins=1440*days
    if option=="min":
        returned=mins
    if option=="date":
        returned=date
    return  returned

"Read in csv of tracker status: columns= date, mode (int)"
def get_csv():
    csv = np.genfromtxt(
            """./data/new_data/sorted/recent_data/mode_2013-2014.csv""",
                         delimiter=",",
                         skip_header=1,dtype=float,
                         converters={0: convert_date},
                         usecols=(0,1)
                          )
    return csv

"split csv into time, mode columns"
def split_csv(csv):
    timestamp = csv[:,0]
    mode = csv[:,1]
#    desc = csv[:,2]
    return timestamp,mode#, desc

"get julian day fraction: to get time of day. 12h00 = .0"
def get_decimal(mjd):
    frac, int_as_float=math.modf(mjd)
    return frac

"Delete the daytime hours from the data"
def delete_day(csv):
    csv=csv[np.logical_not
            (np.logical_or(
                    np.vectorize(get_decimal)(csv[:,0])>0.822,# 0.745 in jd 0.3725 in mjd,
                    np.vectorize(get_decimal)(csv[:,0])<0.229))]# 17.30 in jd, 0.625 in mjd))]
    return csv

csv_start_stop=get_csv()
csv_nights=delete_day(csv_start_stop)
csv_use=csv_nights


durations=[]
blank=[] #list of (start, stop) tuple
track_or_guide=(4,5) #mode int guide or track
for i in range(len(csv_use)):
    if csv_use[:,1][i] in track_or_guide:
        if csv_use[:,1][i-1] not in track_or_guide: #if this time is tracking, but the moment before wasn't
            start=csv_use[:,0][i]

    if csv_use[:,1][i] not in track_or_guide:
        if csv_use[:,1][i-1] in track_or_guide: #if this time isn't tracking, but the moment before was
            end = csv_use[:,0][i]     #then the moment of (notbefore) is end of track
            elapsed=end-start
            if minimum_track<elapsed<1: #1 day #0.291667: #7 hrs, max track sanity check
                durations.append(elapsed)#, csv_use[:,1][i]))
#                if elapsed>0.000694444:
                blank.append((start, end))
  

write=False

"write list of start stop tuples to a csv"
if write==True:
    with open(
            """./data/new_data/sorted/write_to/whole_201{}_times_jd_4.csv""".format(year),#, random.randint(0,200)),
            'w') as myfile:
        wr = csv_writer.writer(myfile, delimiter=',',lineterminator='\n')
        wr.writerows(blank)

