
"""
Edited on Wed 22 April

@author: freya
"""

import numpy as np
import julian
from datetime import datetime#, timedelta
import pytz
import math
import matplotlib.pyplot as plt
# import seaborn as sns
# from matplotlib.colors import ListedColormap
import random
# import csv as csv_writer
import pickle


"Changeable variables"
year=9 #201{} 2012/2013/2011 NB change
break_up=[0.00694444,0.0138889]#,0.0416667] #10, 20 , 60 min
break_up_2=[0.0138889,0.0277778,0.0416667] #20, 40, 60
break_up=break_up_2
"Convert date string to Modern Julian Day float"
def convert_date(d_bytes):
    try:
        s = d_bytes.decode('utf-8')
    except:
        s=d_bytes
    tzone=pytz.timezone('Africa/Johannesburg')
    try:
        dt=datetime.strptime(s, "%Y-%m-%d %H:%M:%S.%f")
    except:
        dt=datetime.strptime(s, "%Y-%m-%d %H:%M:%S")
    dt_local=tzone.localize(dt)
    mjd=julian.to_jd(dt_local, fmt="jd")
    return mjd

"Get minutes or or julian date from days"
def day_to_time(days, option="min"):
    date=julian.from_jd(days, fmt="mjd")
    seconds=86400*days
    mins=1440*days
    if option=="min":
        returned=mins
    if option=="date":
        returned=date
    return  returned

"Get datetime from mjd"
def jd_to_date(jd):
    date=julian.from_jd(jd, fmt="mjd")
    return date

"import csv of x-y coordinates NB change"
def get_csv():
    # file_name="x_y_copy_2018-2019"
    csv=[]
    for i in range(1,13):
        print(i)      
        csv_read = np.genfromtxt ('/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/recent_data/xy/x_y_201{}_{}.csv'.format(year,i),
                     delimiter=",",
                     skip_header=1,dtype=float,
                     converters={0: convert_date},
                     usecols=(0,1,2)
                      )
        csv.append(csv_read)
    return csv #list of each month's arrays

"Get start end indexes from csv written by 'get_track_start_end.py' NB change"
def get_start_end():
    pwd="/home/freya/programming/MiniTrackers/danny/new"
    start_end = np.genfromtxt(
            """{}/data/new_data/sorted/write_to/whole_201{}_times_jd_4.csv""".format(pwd,year),# all_or_not),
                         delimiter=",",
                         skip_header=0,dtype=float,
                         usecols=(0,1)
                          )
    return start_end

def get_az():
    pwd="/home/freya/programming/MiniTrackers/danny/new"
    az = np.genfromtxt(
            """{}/data/new_data/sorted/recent_data/az_2018-2019.csv""".format(pwd),# all_or_not),
                         delimiter=",",
                         skip_header=1,dtype=float,
                         converters={0: convert_date},
                         usecols=(0,1,2)
                          )
    return az

def get_radec():
    pwd="/home/freya/programming/MiniTrackers/danny/new"
    ra_dec = np.genfromtxt(
            """{}/data/new_data/sorted/recent_data/ra_dec_2018-2019.csv""".format(pwd),# all_or_not),
                         delimiter=",",
                         skip_header=1,dtype=float,
                         converters={0: convert_date},
                         usecols=(0,1,2)
                          )
    return ra_dec

"Split csv by columns, bit unnecessary"
def split_csv(csv):
    timestamp = csv[:,0]
#    rho = csv[:,1]
    x = csv[:,1]
    y = csv[:,2]
    return timestamp,x,y#rho, x,y

"Get numbers after decimal point of MJD, i.e. time of dat"
def get_decimal(mjd, frac='frac'):
    frac, int_as_float=math.modf(mjd)
    if frac=='frac':
        return frac
    else:
        return frac
    
"Removed the data that takes place between the latest sunrise and earliest sunset"
def delete_day(csv):
    csv=csv[np.logical_not
            (np.logical_or(
                    np.vectorize(get_decimal)(csv[:,0])>0.822,#32, # 07h45 winter sunrise
                    np.vectorize(get_decimal)(csv[:,0])<=0.25))]#72))] # 17h30 winter sunset #18:00
    return csv


"Count number of science nights in the data"
def count_nights():
    count=0
    days=[]
    for i in range(len(index_starts)-1):
#        print(i)
        if index_starts[i+1]<len(date_tracking): #ignore idices where no xy data
            if int(date_tracking[index_starts[i]])!=int(date_tracking[index_starts[i+1]]): #if this indice's date doesn't equal the next's date, only uses the last index of the day
                days.append(date_tracking[index_starts[i]])
                count+=1
    return count, days

def semesters(year,csv_use,semester=1):
    if semester==1:
        start_date=convert_date("201{}-05-01 12:00:00".format(year))
        end_date=convert_date("201{}-11-01 12:00:00".format(year))
    else:
        start_date=convert_date("201{}-11-01 12:00:00".format(year))
        end_date=convert_date("20{}-05-01 12:00:00".format(year+1))
    start=np.searchsorted(csv_use[:,0], start_date)
    end=np.searchsorted(csv_use[:,0], end_date)
    csv_new=csv_use[start: end,]
    return csv_new
"""////////////////////////////////////////////////////////"""

"This takes the longest"
csv =get_csv()

"add all arrays together"
new_csv=csv[0]
for i in range(1,12):
    new_csv=np.append(new_csv, csv[i], axis=0)

"save this csv as a name that won't be overwritten"    
# csv_save=new_csv
csv=new_csv

"Get tracking start end times from csv"    
start_end=get_start_end()
starts=start_end[:,0]
ends=start_end[:,1]

"Delete day"
csv_nights=delete_day(csv)
csv_better=csv_nights

"Terrible way to get split the data into 2 week periods"
csv_use = csv_better#np.array(csv_better)[:int(0.48*len(csv_better))]#:int(0.855*len(csv_better))]#:int(0.855*len(csv_better))]#:int(1.7*portion*len(csv_better))] #use every  data point
# csv_use=semesters(year, csv_use)

"csv split into columns"
timestamp,x,y=csv_use[:,0], csv_use[:,1], csv_use[:,2]#split_csv(csv_use)

try:
    az=get_az()
    ra_dec=get_radec()
except:
    print("didn't work")
    az=False
    ra_dec=False



"round of julian dates to sort easier"
starts_secs=np.around(starts, decimals=5)
ends_secs=np.around(ends, decimals=5)
csv_secs=np.around(csv_use[:,0], decimals=5)
try:
    az_secs=np.around(az[:,0], decimals=5)
    ra_dec_secs=np.around(ra_dec[:,0], decimals=5)
except:
    pass
"indices of csv_secs which are start times, end times"
index_starts=np.searchsorted(csv_secs, starts_secs)
index_ends=np.searchsorted(csv_secs, ends_secs)
try:
    index_az_start=np.searchsorted(az_secs, starts_secs)
    index_az_end=np.searchsorted(az_secs, ends_secs)
    index_ra_start=np.searchsorted(ra_dec_secs, starts_secs)
    index_ra_end=np.searchsorted(ra_dec_secs, ends_secs)
except:
    pass
x_tracking=x
y_tracking=y
date_tracking=timestamp
try:
    ra=ra_dec[:,2]
    dec=ra_dec[:,1]
except:
    pass
"Get start, end date and number of science nights for plots"
start_stamp=julian.from_jd(timestamp[0], fmt="jd")
end_stamp=julian.from_jd(timestamp[-1], fmt="jd")
nights,_=count_nights()


"0: less than 10 min, 1: 10-20 min, 2: >20 min"
ind_20={}   #not split up into lengths
x_lengths={} #dictionary of indices
y_lengths={}
for k in range(len(break_up)+1):
    x_lengths[k]={}
    y_lengths[k]={}

"get indicies into dicts"
for i in range(len(index_starts)-1):
    if  index_ends[i]<len(date_tracking)-1: #limit indices within dates we have xy data for
        if index_starts[i+1]!=index_starts[i]:  #ignore duplicate indices
            ind_20[str(i)]=[]
            # for j in range(index_starts[i],index_ends[i], 20):
            #     ind_20[str(i)].append(j)
    #        print(index_ends[i]-index_starts[i])
            for k in range(len(break_up)+1):    
                if k==0:
                    if date_tracking[index_ends[i]]-date_tracking[index_starts[i]]<= break_up[k]: #less than 10min
                        x_lengths[k][str(i)]=[] #create list
                        for j in range(index_starts[i],index_ends[i]): #use index every 20th index between start and end
                            # print(j)
                            # if  isinstance(j, int):
                            #     print(j, type(j))
                            # x_lengths[k][str(i)].append(index_starts[i])
                            x_lengths[k][str(i)].append(j)
                        if len(x_lengths[k][str(i)])==0:
                            print(index_starts[i], index_ends[i])
                        # else:
                        #     if (index_ends[i]-10)>index_starts[i]:
                        #         x_lengths[k][str(i)].append(index_ends[i]-10)
                elif k==(len(break_up)):
                    if date_tracking[index_ends[i]]-date_tracking[index_starts[i]]> break_up[k-1]: #more thn 1 hr
                        x_lengths[k][str(i)]=[]
                        for j in range(index_starts[i],index_ends[i],2):
                            # if not isinstance(j, int):
                            #     print(j, type(j))
                            # x_lengths[k][str(i)].append(index_starts[i])
                            x_lengths[k][str(i)].append(j)
                        # if (index_ends[i]-10)>index_starts[i]:
                        #     x_lengths[k][str(i)].append(index_ends[i]-10)
                else:
                    if break_up[k-1]<date_tracking[index_ends[i]]-date_tracking[index_starts[i]]<= break_up[k]: #betw 10min and 20min and 20min and 1hr
                        x_lengths[k][str(i)]=[]
                        for j in range(index_starts[i],index_ends[i],2):
                            # if not isinstance(j, int):
                            #     print(j, type(j))
                            # x_lengths[k][str(i)].append(index_starts[i])
                            x_lengths[k][str(i)].append(j)
                        # if (index_ends[i]-10)>index_starts[i]:
                            # x_lengths[k][str(i)].append(index_ends[i]-10)


# az_dome_lengths={} #dictionary of indices
# az_struc_lengths={}
# az_date_tracking=az[:,0]
# for k in range(len(break_up)+1):
#     az_dome_lengths[k]={}
#     az_struc_lengths[k]={}

# ""
# for i in range(len(index_az_start)-1):
#     if  index_az_end[i]<len(az_date_tracking)-1: #limit indices within dates we have xy data for
#         if index_az_start[i+1]!=index_az_start[i]:  #ignore duplicate indices
#             # ind_20[str(i)]=[]
#             # for j in range(index_starts[i],index_ends[i], 20):
#             #     ind_20[str(i)].append(j)
#     #        print(index_ends[i]-index_starts[i])
#             for k in range(len(break_up)+1):    
#                 if k==0:
#                     if az_date_tracking[index_az_end[i]]-az_date_tracking[index_az_start[i]]<= break_up[k]: #less than 10min
#                         az_dome_lengths[k][str(i)]=[] #create list
#                         for j in range(index_az_start[i],index_az_end[i]): #use index every 20th index between start and end
#                             # print(j)
#                             # if  isinstance(j, int):
#                             #     print(j, type(j))
#                             # x_lengths[k][str(i)].append(index_starts[i])
#                             az_dome_lengths[k][str(i)].append(j)
#                         if len(az_dome_lengths[k][str(i)])==0:
#                             print(index_az_start[i], index_az_end[i])
#                         # else:
#                         #     if (index_ends[i]-10)>index_starts[i]:
#                         #         x_lengths[k][str(i)].append(index_ends[i]-10)
#                 elif k==(len(break_up)):
#                     if az_date_tracking[index_az_end[i]]-az_date_tracking[index_az_start[i]]> break_up[k-1]: #more thn 1 hr
#                         az_dome_lengths[k][str(i)]=[]
#                         for j in range(index_az_start[i],index_az_end[i],2):
#                             # if not isinstance(j, int):
#                             #     print(j, type(j))
#                             # x_lengths[k][str(i)].append(index_starts[i])
#                             az_dome_lengths[k][str(i)].append(j)
#                         # if (index_ends[i]-10)>index_starts[i]:
#                         #     x_lengths[k][str(i)].append(index_ends[i]-10)
#                 else:
#                     if break_up[k-1]<az_date_tracking[index_az_end[i]]-az_date_tracking[index_az_start[i]]<= break_up[k]: #betw 10min and 20min and 20min and 1hr
#                         az_dome_lengths[k][str(i)]=[]
#                         for j in range(index_az_start[i],index_az_end[i],2):
#                             # if not isinstance(j, int):
#                             #     print(j, type(j))
#                             # x_lengths[k][str(i)].append(index_starts[i])
#                             az_dome_lengths[k][str(i)].append(j)
#                         # if (index_ends[i]-10)>index_starts[i]:
#                         #     x_lengths[k][str(i)].append(index_ends[i]-10)


x_dict={}
y_dict={}
date_track_dict={}
x_track_dict={"year":year, "version":"up to next mode, jd_4"}
y_track_dict={}
az_one_dict={}
ra_one_dict={}
dec_one_dict={}

for k in range(len(break_up)+1):
    x_track_dict[k]={}
    y_track_dict[k]={}
    date_track_dict[k]={}
    az_one_dict[k]={}
    ra_one_dict[k]={}
    dec_one_dict[k]={}
    for i in x_lengths[k]:
        try:
            x_lower=-1.61
            x_upper=-1.6204
            y_lower=-1.5005
            y_upper=-1.49
            x_end=x_tracking[np.array(x_lengths[k][i])][-1]
            x_start=x_tracking[np.array(x_lengths[k][i])][0]
            y_end=y_tracking[np.array(x_lengths[k][i])][-1]
            y_start=y_tracking[np.array(x_lengths[k][i])][0]
            if y_end>y_upper and x_end>x_lower and y_start>y_upper and x_start>x_lower:
                
                x_track_dict[k][i]=x_tracking[np.array(x_lengths[k][i])] #filter x_tracking by indices
                y_track_dict[k][i]=y_tracking[np.array(x_lengths[k][i])]
                date_track_dict[k][i]=date_tracking[np.array(x_lengths[k][i])]
                try:
                    ra_one_dict[k][i]=ra[np.searchsorted(ra_dec[:,0],date_track_dict[k][i] )]
                    dec_one_dict[k][i]=dec[np.searchsorted(ra_dec[:,0],date_track_dict[k][i] )]
                    
                except:
                    print("ra dec fail")
                try:
                    print("trying")
                    az_value=az[np.searchsorted(az[:,0],date_track_dict[k][i][0] )][2] #column
                    plus_one=az[np.searchsorted(az[:,0],date_track_dict[k][i][0] )+1]
                    minus_one=az[np.searchsorted(az[:,0],date_track_dict[k][i][0] )-1]
                    if az_value >=360:
                        print("too big")
                        az_value=az_value-360
                    elif az_value<0:
                        print("too small")
                        az_value=360+az_value
                    az_one_dict[k][i]=az_value
                    print("completed")
                except:
                    print("failed")
                    pass
        except:
            pass
# for i in ind_20:
#     x_dict[i]=x_tracking[np.array(ind_20[i])]
#     y_dict[i]=y_tracking[np.array(ind_20[i])]



# az_date_dict={}
# az_dome_track_dict={"year":year, "version":"up to next mode, jd_4"}
# az_struc_track_dict={}
# for k in range(len(break_up)+1):
#     az_dome_track_dict[k]={}
#     az_struc_track_dict[k]={}
#     az_date_dict[k]={}
#     for i in az_dome_lengths[k]:
#         try:
#             az_dome_track_dict[k][i]=az[:,1][np.array(az_dome_lengths[k][i])] #filter x_tracking by indices
#             az_struc_track_dict[k][i]=az[:,2][np.array(az_dome_lengths[k][i])]
#             az_date_dict[k][i]=az[:,0][np.array(az_dome_lengths[k][i])]
#         except:
#             pass


nights,_=count_nights()
print(nights)
print(start_stamp, end_stamp)
write=False
"4 graphs"
if write==True:
    colors=['cornflowerblue', 'cornflowerblue','lightpink','darksalmon', 'r']
    colors2=['cornflowerblue', 'b','r','darksalmon', 'r']
    fig, ((ax1,ax2),(ax3,ax4)) = plt.subplots(2,2)
    axes=(ax1,ax2,ax3,ax4)
    #times=["<10", "<20", "<60", ">60"]
    times=["","10-20min", ">20 min"]
    # count=0
    tot_count=0
    for k in range(len(break_up)+1):
        count=0
        for i in x_track_dict[k]:
            ax=ax1
            if tot_count>420:
                ax=ax2
            if tot_count>840:
                ax=ax3
            if tot_count>1260:
                ax=ax4
            # if tot_count>1000:
                # break
            if count==0:
                ax.plot(x_track_dict[k][i][0:-1], y_track_dict[k][i][0:-1],
                    color=colors[k],linewidth=0.2,alpha=0.65, label=times[k])#, s=0.1)
                ax.scatter(x_track_dict[k][i][0], y_track_dict[k][i][0],
                    color=colors2[k],s=0.2,alpha=(0.65), marker=".")
            else:
                ax.plot(x_track_dict[k][i][0:-1], y_track_dict[k][i][0:-1],
                    color=colors[k],linewidth=0.2,alpha=(0.65))#/(k+1)+0.4))#, s=0.1)
                ax.scatter(x_track_dict[k][i][0], y_track_dict[k][i][0],
                           color=colors2[k],s=0.2,alpha=(0.65), marker=".")
                count+=1
                plt.show()
            count+=1
            tot_count+=1
            # if count==15:
            #     break
    for ax in axes:
        ax.set_xlabel("x")
        ax.set_ylabel("y")
        ax.set_ylim(-1.75,1.75)
        ax.set_xlim( 1.75, -1.75)
        ax.set_aspect(1./ax.get_data_ratio())
        try:
            leg=ax.legend(fancybox=True, framealpha=0.5,
                          bbox_to_anchor=(1.05, 1), loc='upper left',
                          borderaxespad=0.,
                          fontsize='xx-small')
            for l in leg.get_lines():
                l.set_linewidth(4.0)
                l.set_alpha(1)
        except:
            pass
    fig.suptitle("Tracks on {} nights in 201{}".format(nights, year))

"Single graph"
if write==True:
    colors=['cornflowerblue', 'cornflowerblue','lightpink','darksalmon', 'r']
    colors2=['cornflowerblue', 'b','r','darksalmon', 'r']
    fig, ax = plt.subplots(1,1)
    # axes=(ax1,ax2,ax3,ax4)
    times=["<10", "<20", "<60", ">60"]
    # times=["","10-20min", ">20 min"]
    times=["<20","20-40", "40-60", ">60"]
    # count=0
    tot_count=0
    
    for k in [1,2,3]:
        count=0
        for i in x_track_dict[k]:
            # if count==10:
            #     break
            
            if count==0:
                ax.plot(x_track_dict[k][i][0:-1], y_track_dict[k][i][0:-1],
                    color=colors[k],linewidth=0.2,alpha=0.45, label=times[k])#, s=0.1)
                ax.scatter(x_track_dict[k][i][0], y_track_dict[k][i][0],
                           color=colors2[k],s=0.2,alpha=(0.65), marker=".")
            else:
                ax.plot(x_track_dict[k][i][0:-1], y_track_dict[k][i][0:-1],
                    color=colors[k],linewidth=0.2,alpha=(0.45))#/(k+1)+0.4))#, s=0.1)
                ax.scatter(x_track_dict[k][i][0], y_track_dict[k][i][0],
                           color=colors2[k],s=0.2,alpha=(0.65), marker=".")
                count+=1
                plt.show()
            count+=1
            tot_count+=1
            
    ax.set_xlabel("x")
    ax.set_ylabel("y")
    ax.set_ylim(-1.75,1.75)
    ax.set_xlim( 1.75, -1.75)
    ax.set_aspect(1./ax.get_data_ratio())
    try:
        leg=ax.legend(fancybox=True, framealpha=0.5,
                      bbox_to_anchor=(1.05, 1), loc='upper left',
                      borderaxespad=0.,
                      fontsize='xx-small')
        for l in leg.get_lines():
            l.set_linewidth(4.0)
            l.set_alpha(1)
    except:
        pass
    fig.suptitle("Tracks on {} nights in 201{}".format(nights, year))

"Az graph"
if write==True:
    colors=['cornflowerblue', 'cornflowerblue','lightpink','darksalmon', 'r']
    
    fig, ax = plt.subplots(1,1)
    # axes=(ax1,ax2,ax3,ax4)
    #times=["<10", "<20", "<60", ">60"]
    times=["","10-20min", ">20 min"]
    # count=0
    tot_count=0
    
    for k in range(1,len(break_up)+1):
        count=0
        for i in az_dome_track_dict[k]:
            if count==10:
                break
            
            if count==0:
                ax.scatter(az_dome_track_dict[k][i][0:-1], az_struc_track_dict[k][i][0:-1],
                    color=colors[k],s=0.25,alpha=0.25, label=times[k])#, s=0.1)
            else:
                ax.scatter(az_dome_track_dict[k][i][0:-1], az_struc_track_dict[k][i][0:-1],
                    color=colors[k],s=0.25,alpha=(0.25))#/(k+1)+0.4))#, s=0.1)
                count+=1
                plt.show()
            count+=1
            tot_count+=1
            
    # ax.set_xlabel("x")
    # ax.set_ylabel("y")
    # ax.set_ylim(-1.75,1.75)
    # ax.set_xlim( 1.75, -1.75)
    ax.set_aspect(1./ax.get_data_ratio())
    try:
        leg=ax.legend(fancybox=True, framealpha=0.5,
                      bbox_to_anchor=(1.05, 1), loc='upper left',
                      borderaxespad=0.,
                      fontsize='xx-small')
        for l in leg.get_lines():
            l.set_linewidth(4.0)
            l.set_alpha(1)
    except:
        pass
    fig.suptitle("Tracks on {} nights in 201{}".format(nights, year))

"az track graph"
if write==True:
    try:
        colors=['cornflowerblue', 'cornflowerblue','lightpink','darksalmon', 'r']
        colors2=['cornflowerblue', 'b','r','darksalmon', 'r']
        az_list=[]
        fig, ((ax1,ax2),(ax3,ax4)) = plt.subplots(2,2)
        axes=(ax1,ax2,ax3,ax4)
        #times=["<10", "<20", "<60", ">60"]
        times=["","10-20min", ">20 min"]
        # count=0
        tot_count=0
        dome_or_str=2
        for k in range(len(break_up)+1):
            count=0
            for i in x_track_dict[k]:
                az_value_i=az_one_dict[k][i]
                az_list.append(az_value_i)
                if az_value_i<=45 or az_value_i>315:
                    ax=ax1
                elif az_value_i>45 and az_value_i<=135:
                    ax=ax2
                elif az_value_i>135 and az_value_i<=225:
                    ax=ax4
                elif az_value_i>225 and az_value_i<=315:
                    ax=ax3
                if count==0:
                    ax.plot(x_track_dict[k][i][0:-1], y_track_dict[k][i][0:-1],
                        color=colors[k],linewidth=0.2,alpha=0.65, label=times[k])#, s=0.1)
                    ax.scatter(x_track_dict[k][i][0], y_track_dict[k][i][0],
                        color=colors2[k],s=0.2,alpha=0.65, marker=".")
                else:
                    ax.plot(x_track_dict[k][i][0:-1], y_track_dict[k][i][0:-1],
                        color=colors[k],linewidth=0.2,alpha=(0.65))#/(k+1)+0.4))#, s=0.1)
                    ax.scatter(x_track_dict[k][i][0], y_track_dict[k][i][0],
                        color=colors2[k],s=0.2,alpha=(0.65), marker=".")
                    count+=1
                    # plt.show()
                count+=1
                tot_count+=1
                # if count==15:
                #     break
        for ax in axes:
            # ax.set_xlabel("x")
            # ax.set_ylabel("y")
            ax.set_ylim(-1.75,1.75)
            ax.set_xlim( 1.75, -1.75)
            ax.set_aspect(1./ax.get_data_ratio())
            try:
                leg=ax.legend(fancybox=True, framealpha=0.5,
                              bbox_to_anchor=(1.05, 1), loc='upper left',
                              borderaxespad=0.,
                              fontsize='xx-small')
                for l in leg.get_lines():
                    l.set_linewidth(4.0)
                    l.set_alpha(1)
            except:
                pass
        ax1.set_title("315 to 45")
        ax2.set_title("45 to 135")
        ax4.set_title("135 to 225")
        ax3.set_title("225 to 315")
        
        ax1.set_ylabel("y", fontsize='x-small')
        ax3.set_ylabel("y", fontsize='x-small')
        ax3.set_xlabel("x", fontsize='x-small')
        ax4.set_xlabel("x", fontsize='x-small')
        fig.suptitle("Tracks by Structure Azimuth 201{}".format( year))
        plt.show()
        fig, ax=plt.subplots()
        az_hist, az_bins=np.histogram(az_list, bins=60)
        ax.bar(az_bins[:-1], az_hist, width=3, color="cornflowerblue")
        # ax.set_xlim(315,
        ax.set_xticks((0,45,90,135,180,225,270,315))
        ax.set_ylabel("Count (tracks)")
        ax.set_xticklabels(("0", "45", "90", "135", "180", "225", "270", "315"))
        ax.set_title("Structure azimuth when tracking, 201{}".format(year))
    except:
        pass

"2 months"
if write==True:
    colors=['lightblue', 'lightgreen','moccasin','lightpink', 'lightgrey']
    colors2=['b', 'green','orange','pink', 'grey']
    times=["<20min", "20-40min", "40-60min", ">60min"]
    dates=[convert_date("201{}-07-01 12:00:00".format(year)),
           convert_date("201{}-09-01 12:00:00".format(year))]
    # count=0
    tot_count=0
    fig1,ax1=plt.subplots()
    fig2,ax2=plt.subplots()
    fig3,ax3=plt.subplots()
    axes=(ax1,ax2,ax3)
    for k in [3,2,1,0]:
        count=0
        for i in x_track_dict[k]:        
            date=date_track_dict[k][i]
            if date[0]<dates[0]:
                ax=ax1
                ax.set_title("201{} Semester 1, May/Jun".format(year))
            if date[0]>dates[0] and date[0]<dates[1]:
                ax=ax2
                ax.set_title("201{} Semester 1, Jul/Aug".format(year))
            if date[0]>dates[1]:
                ax=ax3
                ax.set_title("201{} Semester 1, Sept/Oct".format(year))
            # if tot_count>1000:
                # break
            if count==0:
                ax.plot(x_track_dict[k][i][0:-1], y_track_dict[k][i][0:-1],
                    color=colors[k],linewidth=0.2,alpha=0.65, label=times[k])#, s=0.1)
                ax.scatter(x_track_dict[k][i][0], y_track_dict[k][i][0],
                    color=colors2[k],s=0.2,alpha=(0.65), marker=".")
            else:
                ax.plot(x_track_dict[k][i][0:-1], y_track_dict[k][i][0:-1],
                    color=colors[k],linewidth=0.2,alpha=(0.65))#/(k+1)+0.4))#, s=0.1)
                ax.scatter(x_track_dict[k][i][0], y_track_dict[k][i][0],
                           color=colors2[k],s=0.2,alpha=(0.65), marker=".")
                count+=1
                plt.show()
            count+=1
            tot_count+=1
            # if count==15:
            #     break
    # for ax in axes:
        
            ax.set_xlabel("x")
            ax.set_ylabel("y")
            ax.set_ylim(-1.75,1.75)
            ax.set_xlim( 1.75, -1.75)
            ax.set_aspect(1./ax.get_data_ratio())
            try:
                lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
                labels = times
                # plt.legend(lines, labels)
                leg=ax.legend(lines, labels, fancybox=True, framealpha=0.5,
                              bbox_to_anchor=(1.05, 1), loc='upper left',
                              borderaxespad=0.,
                              fontsize='xx-small')
                # for l in leg.get_lines():
                #     l.set_linewidth(4.0)
                #     l.set_alpha(1)
            except:
                pass
    # fig.suptitle("Tracks on {} nights in 201{}".format(nights, year))

"az 45 deg bins"
if write==True:
    try:
        colors=['lightblue', 'lightgreen','moccasin','lightpink', 'lightgrey']
        colors2=['b', 'green','orange','pink', 'grey']
        times=["<20min", "20-40min", "40-60min", ">60min"]
        az_list=[]
        # fig1,ax1=plt.subplots()
        # fig2,ax2=plt.subplots()
        # fig3,ax3=plt.subplots()
        # fig4,ax4=plt.subplots()
        fig5,ax5=plt.subplots()
        fig6,ax6=plt.subplots()
        fig7,ax7=plt.subplots()
        fig8,ax8=plt.subplots()
        axes=[ax5,ax6,ax7,ax8]#ax1,ax3,ax3,ax4]#,ax5,ax6,ax7,ax8]
        #times=["<10", "<20", "<60", ">60"]
        # times=["","10-20min", ">20 min"]
        # count=0
        tot_count=0
        dome_or_str=2
        for k in [3,2,1,0]:
            count=0
            for i in x_track_dict[k]:
                az_value_i=az_one_dict[k][i]
                az_list.append(az_value_i)
                # if az_value_i<=45:
                #     ax=ax1
                #     title="0-45"
                # elif az_value_i>45 and az_value_i<=90:
                #     ax=ax2
                #     title="45-90"
                # elif az_value_i>90 and az_value_i<=135:
                #     ax=ax3
                #     title="90-135"
                # elif az_value_i>135 and az_value_i<=180:
                #     ax=ax4
                #     title="135-180"
                if az_value_i<=180:
                    print("1")
                elif az_value_i>180 and az_value_i<=225:
                    ax=ax5
                    title="180-225"
                elif az_value_i>225 and az_value_i<=270:
                    ax=ax6
                    title="225-270"
                elif az_value_i>270 and az_value_i<=315:
                    ax=ax7
                    title="270-315"
                else:
                    ax=ax8
                    title="315-360"
                ax.set_title("201{} Semester 1, azimuth: {} deg".format(year, title))
                if count==0:
                    ax.plot(x_track_dict[k][i][0:-1], y_track_dict[k][i][0:-1],
                        color=colors[k],linewidth=0.2,alpha=0.65, label=times[k])#, s=0.1)
                    ax.scatter(x_track_dict[k][i][0], y_track_dict[k][i][0],
                        color=colors2[k],s=0.2,alpha=0.65, marker=".")
                else:
                    ax.plot(x_track_dict[k][i][0:-1], y_track_dict[k][i][0:-1],
                        color=colors[k],linewidth=0.2,alpha=(0.65))#/(k+1)+0.4))#, s=0.1)
                    ax.scatter(x_track_dict[k][i][0], y_track_dict[k][i][0],
                        color=colors2[k],s=0.2,alpha=(0.65), marker=".")
                    count+=1
                    # plt.show()
                count+=1
                tot_count+=1
                # if count==15:
                #     break
        # for ax in axes:
                ax.set_xlabel("x")
                ax.set_ylabel("y")
                ax.set_ylim(-1.75,1.75)
                ax.set_xlim( 1.75, -1.75)
                ax.set_aspect(1./ax.get_data_ratio())
                try:
                    lines = [Line2D([0], [0], color=c, linewidth=3, linestyle='-') for c in colors]
                    labels = times
                    leg=ax.legend(lines, labels,fancybox=True, framealpha=0.5,
                                  bbox_to_anchor=(1.05, 1), loc='upper left',
                                  borderaxespad=0.,
                                  fontsize='xx-small')
                    # for l in leg.get_lines():
                    #     l.set_linewidth(4.0)
                    #     l.set_alpha(1)
                except:
                    pass
        
        # plt.show()
        # fig, ax=plt.subplots()
        # az_hist, az_bins=np.histogram(az_list, bins=60)
        # ax.bar(az_bins[:-1], az_hist, width=3, color="cornflowerblue")
        # # ax.set_xlim(315,
        # ax.set_xticks((0,45,90,135,180,225,270,315))
        # ax.set_ylabel("Count (tracks)")
        # ax.set_xticklabels(("0", "45", "90", "135", "180", "225", "270", "315"))
        # ax.set_title("Structure azimuth when tracking, 201{}".format(year))
    except:
        pass

















"Save to pickle"    
year=9
random_number="june"#"organised_18:00" #random.randint(1,50)
# pickle.dump( csv_use, open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/csv_use_{}_{}.p".format(year,random_number), "wb" ) )
# pickle.dump( x_track_dict, open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/x_track_dict_{}_{}.p".format(year,random_number), "wb" ) )
# pickle.dump( y_track_dict, open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/y_track_dict_{}_{}.p".format(year,random_number), "wb" ) )
# pickle.dump( date_track_dict, open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/date_track_dict_{}_{}.p".format(year,random_number), "wb" ) )
# pickle.dump( index_starts, open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/index_starts_{}_{}.p".format(year,random_number), "wb" ) )
# pickle.dump( index_ends, open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/index_ends_{}_{}.p".format(year,random_number), "wb" ) )
# try:
#     pickle.dump( az_one_dict, open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/az_one_dict_{}_{}.p".format(year,random_number), "wb" ) )
# except:
#     pass

# x_track_dict = pickle.load( open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/x_track_dict_{}_{}.p".format(year,random_number), "rb" ) )
# y_track_dict = pickle.load( open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/y_track_dict_{}_{}.p".format(year,random_number), "rb" ) )
# date_track_dict = pickle.load( open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/date_track_dict_{}_{}.p".format(year,random_number), "rb" ) )
# index_starts=pickle.load(  open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/index_starts_{}_{}.p".format(year,random_number), "rb" ) )
# index_ends=pickle.load(  open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/index_ends_{}_{}.p".format(year,random_number), "rb" ) )
# try:
#     az_one_dict=pickle.load( open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/az_one_dict_{}_{}.p".format(year,random_number), "rb" ) )
# except:
#     pass
# csv_use=pickle.load(open( "/home/freya/programming/MiniTrackers/danny/new/data/new_data/sorted/csv_use_{}_{}.p".format(year,random_number), "rb"))