#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  6 09:18:45 2020

@author: freya
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 14:14:45 2020

@author: freya
"""

import numpy as np
import julian
from datetime import datetime#, timedelta
import pytz
import math
import matplotlib.pyplot as plt
import matplotlib
import matplotlib.animation as animation
from matplotlib.animation import FuncAnimation

import seaborn as sns
from matplotlib.colors import ListedColormap



year=4

def convert_date(d_bytes):
    s = d_bytes.decode('utf-8')
    tzone=pytz.timezone('Africa/Johannesburg')
    try:
        dt=datetime.strptime(s, "%Y-%m-%d %H:%M:%S.%f")
    except:
        dt=datetime.strptime(s, "%Y-%m-%d %H:%M:%S")
    dt_local=tzone.localize(dt)
    mjd=julian.to_jd(dt_local, fmt="mjd")
    return mjd

def day_to_time(days, option="min"):
    date=julian.from_jd(days, fmt="mjd")
    seconds=86400*days
    mins=1440*days
    if option=="min":
        returned=mins
    if option=="date":
        returned=date
    return  returned

def jd_to_date(jd):
    date=julian.from_jd(jd, fmt="mjd")
    return date

def get_csv():
    csv = np.genfromtxt ('/home/freya/programming/MiniTrackers/danny/new/data/tracker_201{}.csv'.format(str(year)),
                         delimiter=",",
                         skip_header=1,dtype=float,
                         converters={0: convert_date},
                         usecols=(0,1,2,3)
                          )
    return csv

def get_start_end():
    start_end = np.genfromtxt(
            '/home/freya/programming/MiniTrackers/danny/new/data/start_stop_times_201{}.csv'.format(year),
                         delimiter=",",
                         skip_header=0,dtype=float,
                         usecols=(0,1)
                          )
    return start_end

def split_csv(csv):
    timestamp = csv[:,0]
    rho = csv[:,1]
    x = csv[:,2]
    y = csv[:,3]
    return timestamp,rho, x,y

def get_decimal(mjd):
    frac, int_as_float=math.modf(mjd)
    return frac

def delete_day(csv):
    csv=csv[np.logical_not
            (np.logical_and(
                    np.vectorize(get_decimal)(csv[:,0])>0.32, # 07h45 winter sunrise
                    np.vectorize(get_decimal)(csv[:,0])<0.72))] # 17h30 winter sunset
    return csv

#def delete_tracks_paused(csv):
#    csv=csv[np.logical_not(csv[:,4]!=0)]
#    return csv
def delete_guides_paused(csv):
    csv=csv[np.logical_not(csv[:,1]!=0)]
    return csv
#
#def rad_to_deg(array):
#    degrees=np.degrees(array)
#    return degrees
    
csv=get_csv()

start_end=get_start_end()
starts=start_end[:,0]
ends=start_end[:,1]
csv_nights=delete_day(csv)
#csv_tracking=delete_tracks_paused(csv_nights)
#csv_guiding=delete_guides_paused(csv_tracking)
csv_better=csv_nights
csv_use = np.array(csv_better)[::1] #use every 2nd data point

timestamp,rho, x,y=split_csv(csv_use)
#ra_deg=rad_to_deg(ra)
#dec_deg=rad_to_deg(dec)
#time_of_day=np.vectorize(get_decimal)(timestamp)

#start_date=np.vectorize(jd_to_date)(starts)
#end_date=np.vectorize(jd_to_date)(ends)
#csv_date=np.vectorize(jd_to_date)(timestamp)
#margin=1.1574e-5 #5 
#index_start=[]
starts_secs=np.around(starts, decimals=5)
ends_secs=np.around(ends, decimals=5)
csv_secs=np.around(csv_use[:,0], decimals=5)

index_starts=np.searchsorted(csv_secs, starts_secs)
index_ends=np.searchsorted(csv_secs, ends_secs)
#
#all_index=[]
#start_stop=[]
#all_index_end=[]
#for i in range (len(index_starts)-1):
#    if index_starts[i+1]!=index_starts[i]:
#        start_stop.append((index_starts, index_ends))
#        for j in range(index_starts[i],index_ends[i]+1, 1):
#            all_index.append(j)

x_tracking=x
y_tracking=y
date_tracking=timestamp

x_tracking_start=x_tracking[np.array(index_starts)]
x_tracking_end=x_tracking[np.array(index_ends)]
y_tracking_start=y_tracking[np.array(index_starts)]
y_tracking_end=y_tracking[np.array(index_ends)]
date_tracking=date_tracking[np.array(all_index)]
    
cmap = ListedColormap( sns.color_palette("Spectral",10))#, as_cmap=True)#, reverse=True) #colour scheme
fig, ax = plt.subplots()
for i in range(len(x_tracking_end)):
    ax.plot((x_tracking_start[i], x_tracking_end[i]), 
              (y_tracking_start[i],y_tracking_end[i]), 
              s=0.2, c=date_tracking,
                cmap=cmap, alpha=0.5)





#fig=plt.figure()
#ax=fig.add_subplot(111, projection="3d")
#
#ax.scatter(x_tracking, date_tracking, y_tracking, s=0.2,alpha=0.5)
##           c=date_tracking, cmap=cmap, )
#ax.set_xlabel("x")
#ax.set_ylabel("MJD")
#ax.set_zlabel("y")



#title = 'Heroin Overdoses'
#d = get_data(overdoses,18,title)
#x = np.array(d.index)
#y = np.array(d['Heroin Overdoses'])
#overdose =np.column_stack((x_tracking, y_tracking))# pd.DataFrame(y,x)
##XN,YN = augment(x,y,10)
##augmented = pd.DataFrame(YN,XN)
##overdose.columns = {title}
#
#Writer = animation.writers['ffmpeg']
#writer = Writer(fps=20, metadata=dict(artist='Me'), bitrate=1800)
#fig = plt.figure(figsize=(10,6))
#
#def animate(i):
##    data = overdose#.iloc[:int(i+1)] #select data range
#    p = plt.plot(x_tracking,y_tracking)#sns.lineplot(x="x", y="y", data=data, color="r")
#    p.tick_params(labelsize=17)
#    plt.setp(p.lines,linewidth=7)
#ani = matplotlib.animation.FuncAnimation(fig, animate, 
#                                         frames=1700, repeat=True,
#                                         interval=1000, blit=True)
#plt.show() #ani.save('HeroinOverdosesJumpy.mp4', writer=writer)
#
#fig = plt.figure()
#plt.xlim(0, 10)
#plt.ylim(0, 1)
#graph, = plt.plot([], [], 'o')
#
#def animate(i):
#    graph.set_data(x_tracking[:i+1], y_tracking[:i+1])
#    return graph
#
#ani = FuncAnimation(fig, animate, frames=10, interval=200)
#plt.show()