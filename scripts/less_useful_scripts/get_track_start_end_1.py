#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Mar  5 12:31:00 2020

@author: freya
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 14:14:45 2020

@author: freya
"""
import random
import numpy as np
import julian
from datetime import datetime
import pytz
import csv as csv_writer
import math
import matplotlib.pyplot as plt

"CHANGE YEAR HERE"
year=3 #2014 or 2013
minimum_track=0#0.00694444  #10 min in days

def convert_date(d_bytes):
    s = d_bytes.decode('utf-8')
    tzone=pytz.timezone('Africa/Johannesburg')
    try:
        dt=datetime.strptime(s, "%Y-%m-%d %H:%M:%S.%f")
    except:
        dt=datetime.strptime(s, "%Y-%m-%d %H:%M:%S")
    dt_local=tzone.localize(dt)
    mjd=julian.to_jd(dt_local, fmt="mjd")
    return mjd

def day_to_time(days, option="min"):
    date=julian.from_jd(days, fmt="mjd")
    seconds=86400*days
    mins=1440*days
    if option=="min":
        returned=mins
    if option=="date":
        returned=date
    return  returned

def get_csv():
    csv = np.genfromtxt(
            """./data/new_data/sorted/201{}_status.csv""".format(year),
                         delimiter=",",
                         skip_header=1,dtype=float,
                         converters={0: convert_date},
                         usecols=(0,1)
                          )
    return csv


def split_csv(csv):
    timestamp = csv[:,0]
    mode = csv[:,1]
#    desc = csv[:,2]
    return timestamp,mode#, desc

def get_decimal(mjd):
    frac, int_as_float=math.modf(mjd)
    return frac

def delete_day(csv):
    csv=csv[np.logical_not
            (np.logical_and(
                    np.vectorize(get_decimal)(csv[:,0])>0.3725,
                    np.vectorize(get_decimal)(csv[:,0])<0.625))]
    return csv

def only_tracking(csv):
    csv=csv[np.logical_not(csv[:,1]!=4)]
    return csv
    

#def delete_tracks_paused(csv):
#    csv=csv[np.logical_not(csv[:,4]!=0)]
#    return csv
    
csv=get_csv()
csv_nights=delete_day(csv)
#csv_tracking=delete_tracks_paused(csv_nights)
csv_tracking=only_tracking(csv_nights)
csv_use=csv_nights
timestamp,mode=split_csv(csv_tracking)
#timestamp,ra,dec,track_paused=split_csv(csv_nights)
#time=np.vectorize(get_decimal)(timestamp)

durations=[]
blank=[]
track_or_guide=(4,5) #or 5
for i in range(len(csv_use)):
    if csv_use[:,1][i] in track_or_guide:
        if csv_use[:,1][i-1] not in track_or_guide:
            start=csv_use[:,0][i]
#        if csv_use[:,1][i+1] not in track_or_guide:
#            end= csv_use[:,0][i+1]
    if csv_use[:,1][i] not in track_or_guide:
        if csv_use[:,1][i-1] in track_or_guide:
            end = csv_use[:,0][i]
            elapsed=end-start
            if minimum_track<elapsed<1: #1 day #0.291667: #7 hrs, max track
                durations.append(elapsed)#, csv_use[:,1][i]))
#                if elapsed>0.000694444:
                blank.append((start, end))
  


with open(
        """./data/new_data/sorted/write_to/201{}_times_all_{}.csv""".format(year, random.randint(0,200)),
        'w') as myfile:
    wr = csv_writer.writer(myfile, delimiter=',',lineterminator='\n')
    wr.writerows(blank)


#              
#dur_arr=np.array(durations)
#dur_arr=np.vectorize(day_to_time)(dur_arr) #min
#dur_sort=np.sort(dur_arr)
#hist_durations, bin_edges=np.histogram(dur_sort, bins=np.linspace(0,160, 80))
#bin_widths=int(160/80)
#
#fig, ax1=plt.subplots()
#bars=ax1.bar(bin_edges[:-1], hist_durations, width=np.diff(bin_edges), ec="k", align="edge")
#ax1.set_yscale('linear')
#ax1.set_xlabel("Track in Min")
#ax1.set_ylabel("Frequency ")
#bars.set_label("Bins={} mins".format(str(bin_widths)))
#ax1.legend()
##ax1.set_ylim([10,200])
#ax1.set_title("201{}: frequency of track + guide durations".format(year))
#plt.show()
#
#total_duration=np.sum(a=dur_sort, axis=0, dtype=float)
#durations_use=dur_sort#100*(1/total_duration)*dur_sort
#
#hist_durations, bin_edges=np.histogram(durations_use, bins=np.linspace(0,120, 60))
#bin_widths=int(120/60)
#percentage_total=np.true_divide(hist_durations,total_duration )
#y_val=100*hist_durations*(1/total_duration)*bin_edges[1:]
#
#fig, ax1=plt.subplots()
#bars=ax1.bar(bin_edges[:-1], y_val, width=np.diff(bin_edges), ec="k", align="edge")
#ax1.set_yscale('linear')
#ax1.set_xlabel("Track in Min")
#ax1.set_ylabel("Percent of total track time")
#bars.set_label("Bins={} mins".format(str(bin_widths)))
#ax1.legend()
##ax1.set_ylim([10,200])
#ax1.set_title("201{}: Distribution of a year's tracking time".format(year))
#plt.show()
#
#
#
#
#
##plt.scatter(np.arange(len(dur_sort)), dur_sort, s=0.2)
#
##hist_durations_mins, bin_edges_mins=np.histogram(dur_sort, bins=np.linspace(0,400, 100))
##
##fig, ax1=plt.subplots()
##ax1.bar(bin_edges_mins[:-1], hist_durations_mins, width=np.diff(bin_edges_mins), ec="k", align="edge")
##ax1.set_xlim(8,400)
##ax1.set_ylim(0,150)
##plt.show()