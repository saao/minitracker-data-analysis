#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  6 09:18:45 2020

@author: freya
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 14:14:45 2020

@author: freya
"""

import numpy as np
import julian
from datetime import datetime, timedelta
import pytz
import math
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.colors import ListedColormap



year=3

def convert_date(d_bytes):
    s = d_bytes.decode('utf-8')
    tzone=pytz.timezone('Africa/Johannesburg')
    try:
        dt=datetime.strptime(s, "%Y-%m-%d %H:%M:%S.%f")
    except:
        dt=datetime.strptime(s, "%Y-%m-%d %H:%M:%S")
    dt_local=tzone.localize(dt)
    mjd=julian.to_jd(dt_local, fmt="mjd")
    return mjd

def day_to_time(days, option="min"):
    date=julian.from_jd(days, fmt="mjd")
    seconds=86400*days
    mins=1440*days
    if option=="min":
        returned=mins
    if option=="date":
        returned=date
    return  returned

def jd_to_date(jd):
    date=julian.from_jd(jd, fmt="mjd")
    return date

def get_csv():
    csv = np.genfromtxt ('./data/new_data/sorted/201{}_x_y.csv'.format(str(year)),
                         delimiter=",",
                         skip_header=1,dtype=float,
                         converters={0: convert_date},
                         usecols=(0,1,2)
                          )
    return csv


#'/home/freya/programming/MiniTrackers/danny/new/data/start_stop_times_201{}.csv'.format(year),
#            """/home/freya/programming/MiniTrackers/danny/new/data/new_data/write_to/st_end_201{}_{}.csv""".format(year, 140),

def get_start_end():
    start_end = np.genfromtxt(
            """./data/new_data/sorted/write_to/201{}_times.csv""".format(year, 151),
                         delimiter=",",
                         skip_header=0,dtype=float,
                         usecols=(0,1)
                          )
    return start_end

def split_csv(csv):
    timestamp = csv[:,0]
#    rho = csv[:,1]
    x = csv[:,1]
    y = csv[:,2]
    return timestamp,x,y#rho, x,y

def get_decimal(mjd):
    frac, int_as_float=math.modf(mjd)
    return frac

def delete_day(csv):
    csv=csv[np.logical_not
            (np.logical_and(
                    np.vectorize(get_decimal)(csv[:,0])>0.32, # 07h45 winter sunrise
                    np.vectorize(get_decimal)(csv[:,0])<0.72))] # 17h30 winter sunset
    return csv

#def delete_tracks_paused(csv):
#    csv=csv[np.logical_not(csv[:,4]!=0)]
#    return csv
def delete_guides_paused(csv):
    csv=csv[np.logical_not(csv[:,1]!=0)]
    return csv
#
#def rad_to_deg(array):
#    degrees=np.degrees(array)
#    return degrees
    
csv=get_csv()

start_end=get_start_end()
starts=start_end[:,0]
ends=start_end[:,1]
csv_nights=delete_day(csv)
#csv_tracking=delete_tracks_paused(csv_nights)
#csv_guiding=delete_guides_paused(csv_tracking)
csv_better=csv_nights
csv_use = np.array(csv_better)[::1] #use every  data point

timestamp,x,y=split_csv(csv_use)
#ra_deg=rad_to_deg(ra)
#dec_deg=rad_to_deg(dec)
#time_of_day=np.vectorize(get_decimal)(timestamp)

#start_date=np.vectorize(jd_to_date)(starts)
#end_date=np.vectorize(jd_to_date)(ends)
#csv_date=np.vectorize(jd_to_date)(timestamp)
#margin=1.1574e-5 #5 
#index_start=[]
starts_secs=np.around(starts, decimals=5)
ends_secs=np.around(ends, decimals=5)
csv_secs=np.around(csv_use[:,0], decimals=5)

index_starts=np.searchsorted(csv_secs, starts_secs)
index_ends=np.searchsorted(csv_secs, ends_secs)

all_index=[]
#all_index_end=[]
#for i in range (len(index_starts)-1):
#    if index_starts[i+1]!=index_starts[i]:
#        for j in range(index_starts[i],index_ends[i]+1, 10):
#            all_index.append(j)

x_tracking=x
y_tracking=y
date_tracking=timestamp
#x_tracking=x_tracking[np.array(all_index)][::10]
#y_tracking=y_tracking[np.array(all_index)][::10]
#date_tracking=date_tracking[np.array(all_index)][::10]
x_tracking=x
y_tracking=y
ind_20={}
for i in range(len(index_starts)-1):
    if index_starts[i+1]!=index_starts[i]:
        ind_20[str(i)]=[]
        
        for j in range(index_starts[i],index_ends[i]+1, 20):
            ind_20[str(i)].append(j)
x_dict={}
y_dict={}
for i in ind_20:
    x_dict[i]=x_tracking[np.array(ind_20[i])]
    y_dict[i]=y_tracking[np.array(ind_20[i])]
    



#cmap = ListedColormap( sns.color_palette("Spectral",50))#, reverse=True)#, as_cmap=True)#, reverse=True) #colour scheme
#cmap.reversed()
#plt.scatter(x_tracking, y_tracking,s=0.2, c=date_tracking,
#            cmap=cmap, alpha=0.5)
#fig=plt.figure()
#ax=fig.add_subplot(111)#, projection="3d")
fig, ax = plt.subplots()
for i in x_dict:
    ax.plot(x_dict[i], y_dict[i], color='cornflowerblue', alpha=0.5)
#ax.scatter(x_tracking, date_tracking, y_tracking, s=0.2,alpha=0.5)
##           c=date_tracking, cmap=cmap, )
#sc=ax.plot(x_tracking[0:], y_tracking[0:], alpha=0.5)#, s=0.2)#, c=date_tracking, cmap=cmap)
ax.set_xlabel("x")
#ax.set_ylabel("MJD")
#ax.set_zlabel("y")
ax.set_ylabel("y")
ax.set_ylim(-1.75,1.75)
ax.set_xlim(-1.75, 1.75)
ax.set_title("Tracking (>10 min) coordinates 201{}".format(year))
#cbar=fig.colorbar(sc, ticks=[date_tracking[1], date_tracking[-2]])
#cbar.ax.set_yticklabels(['01/12/2013', '22/01/2014'])

