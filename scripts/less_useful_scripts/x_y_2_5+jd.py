#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Mar  6 09:18:45 2020

@author: freya
"""

#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Mar  4 14:14:45 2020

@author: freya
"""

import numpy as np
import julian
from datetime import datetime, timedelta
import pytz
import math
import matplotlib.pyplot as plt
import seaborn as sns
from matplotlib.colors import ListedColormap
import random
import csv as csv_writer


"Changeable variables"
year=3 #201{} 2012/2013/2011
all_or_not="" #_all or '' include <10 min (in _all) or not
break_up=[0.00694444,0.0138889]#,0.0416667] #10, 20 , 60 min
portion=0.4 #halve period?

"Convert date string to Modern Julian Day float"
def convert_date(d_bytes):
    s = d_bytes.decode('utf-8')
    tzone=pytz.timezone('Africa/Johannesburg')
    try:
        dt=datetime.strptime(s, "%Y-%m-%d %H:%M:%S.%f")
    except:
        dt=datetime.strptime(s, "%Y-%m-%d %H:%M:%S")
    dt_local=tzone.localize(dt)
    mjd=julian.to_jd(dt_local, fmt="jd")
    return mjd

"Get minutes or or julian date from days"
def day_to_time(days, option="min"):
    date=julian.from_jd(days, fmt="mjd")
    seconds=86400*days
    mins=1440*days
    if option=="min":
        returned=mins
    if option=="date":
        returned=date
    return  returned

"Get datetime from mjd"
def jd_to_date(jd):
    date=julian.from_jd(jd, fmt="mjd")
    return date

"import csv of x-y coordinates"
def get_csv():
    file_name="x_y_copy_2018-2019"
    csv = np.genfromtxt ('./data/new_data/sorted/recent_data/{}.csv'.format(file_name),
                         delimiter=",",
                         skip_header=1,dtype=float,
                         converters={0: convert_date},
                         usecols=(0,1,2)
                          )
    return csv

"Get start end indexes from csv written by 'get_track_start_end.py'"
def get_start_end():
    start_end = np.genfromtxt(
            """./data/new_data/sorted/write_to/201{}_times_jd.csv""".format(year),# all_or_not),
                         delimiter=",",
                         skip_header=0,dtype=float,
                         usecols=(0,1)
                          )
    return start_end

"Split csv by columns, bit unnecessary"
def split_csv(csv):
    timestamp = csv[:,0]
#    rho = csv[:,1]
    x = csv[:,1]
    y = csv[:,2]
    return timestamp,x,y#rho, x,y

"Get numbers after decimal point of MJD, i.e. time of dat"
def get_decimal(mjd, frac='frac'):
    frac, int_as_float=math.modf(mjd)
    if frac=='frac':
        return frac
    else:
        return frac
    
"Removed the data that takes place between the latest sunrise and earliest sunset"
def delete_day(csv):
    csv=csv[np.logical_not
            (np.logical_or(
                    np.vectorize(get_decimal)(csv[:,0])>0.822,#32, # 07h45 winter sunrise
                    np.vectorize(get_decimal)(csv[:,0])<0.229))]#72))] # 17h30 winter sunset
    return csv


"Count number of science nights in the data"
def count_nights():
    count=0
    days=[]
    for i in range(len(index_starts)-1):
#        print(i)
        if index_starts[i+1]<len(date_tracking):
            if int(date_tracking[index_starts[i]])!=int(date_tracking[index_starts[i+1]]):
                days.append(date_tracking[index_starts[i]])
                count+=1
    return count, days
"""////////////////////////////////////////////////////////"""

"This takes the longest"
csv=get_csv()

# start_end=get_start_end()
# starts=start_end[:,0]
# ends=start_end[:,1]
csv_nights=delete_day(csv)

csv_better=csv_nights

"Terrible way to get split the data into 2 week periods"
csv_use = csv_better#np.array(csv_better)[:int(0.48*len(csv_better))]#:int(0.855*len(csv_better))]#:int(0.855*len(csv_better))]#:int(1.7*portion*len(csv_better))] #use every  data point
write=False

#


timestamp,x,y=split_csv(csv_use)

# starts_secs=np.around(starts, decimals=5)
# ends_secs=np.around(ends, decimals=5)
# csv_secs=np.around(csv_use[:,0], decimals=5)

# index_starts=np.searchsorted(csv_secs, starts_secs)
# index_ends=np.searchsorted(csv_secs, ends_secs)

# all_index=[]
# #all_index_end=[]
# #for i in range (len(index_starts)-1):
# #    if index_starts[i+1]!=index_starts[i]:
# #        for j in range(index_starts[i],index_ends[i]+1, 10):
# #            all_index.append(j)

# x_tracking=x
# y_tracking=y
# date_tracking=timestamp
# start_stamp=julian.from_jd(timestamp[0], fmt="jd")
# end_stamp=julian.from_jd(timestamp[-1], fmt="jd")
# nights,_=count_nights()

# ind_20={}
# x_lengths={}
# y_lengths={}
# for k in range(len(break_up)+1):
#     x_lengths[k]={}
#     y_lengths[k]={}

# for i in range(len(index_starts)-1):
#     if  index_ends[i]<len(date_tracking)-1:
#         if index_starts[i+1]!=index_starts[i]:
#             ind_20[str(i)]=[]
#             for j in range(index_starts[i],index_ends[i]+1, 20):
#                 ind_20[str(i)].append(j)
#     #        print(index_ends[i]-index_starts[i])
#             for k in range(len(break_up)+1):
#                 if k==0:
#                     if date_tracking[index_ends[i]]-date_tracking[index_starts[i]]<= break_up[k]: #less than 10min
#                         x_lengths[k][str(i)]=[]
#                         for j in range(index_starts[i],index_ends[i]+1, 20):
#                             x_lengths[k][str(i)].append(j)
#                 elif k==(len(break_up)):
#                     if date_tracking[index_ends[i]]-date_tracking[index_starts[i]]> break_up[k-1]: #more thn 1 hr
#                         x_lengths[k][str(i)]=[]
#                         for j in range(index_starts[i],index_ends[i]+1, 20):
#                             x_lengths[k][str(i)].append(j)
#                 else:
#                     if break_up[k-1]<date_tracking[index_ends[i]]-date_tracking[index_starts[i]]<= break_up[k]: #betw 10min and 20min and 20min and 1hr
#                         x_lengths[k][str(i)]=[]
#                         for j in range(index_starts[i],index_ends[i]+1, 20):
#                             x_lengths[k][str(i)].append(j)

# x_dict={}
# y_dict={}

# x_track_dict={}
# y_track_dict={}
# for k in range(len(break_up)+1):
#     x_track_dict[k]={}
#     y_track_dict[k]={}
    
#     for i in x_lengths[k]:
#         x_track_dict[k][i]=x_tracking[np.array(x_lengths[k][i])]
#         y_track_dict[k][i]=y_tracking[np.array(x_lengths[k][i])]
# for i in ind_20:
#     x_dict[i]=x_tracking[np.array(ind_20[i])]
#     y_dict[i]=y_tracking[np.array(ind_20[i])]
# #for i in ind_longer:
# ##    print(ind_longer[i])
# #    x_long[i]=x_tracking[np.array(ind_longer[i])]#x_tracking[np.array(ind_longer[i])]
# #    y_long[i]=y_tracking[np.array(ind_longer[i])]
# #for i in ind_shorter:
# #    x_short[i]=x_tracking[np.array(ind_shorter[i])]
# #    y_short[i]=y_tracking[np.array(ind_shorter[i])]


# nights,_=count_nights()
# print(nights)
# print(start_stamp, end_stamp)
# write=False
# if write==True:
#     with open(
#             """./data/new_data/sorted/write_to/split_201{}_{}_{}_to_{}_v_{}.csv""".format(
#                     year, nights, 
#                                                         start_stamp.date(), 
#                                                         end_stamp.date(),
#                                                         random.randint(0,200)),
#             'w') as myfile:
#         wr = csv_writer.writer(myfile, delimiter=',',lineterminator='\n')
#         wr.writerows(csv_use)
        
    
#     colors=['cornflowerblue', 'cornflowerblue','thistle','darksalmon', 'r']
    
    
#     fig, ax = plt.subplots()
#     #times=["<10", "<20", "<60", ">60"]
#     times=["","10-20min", ">20 min"]
#     for k in range(len(break_up)+1):
#         count=0
#         for i in x_track_dict[k]:
#     #        print(type(i), i)
#             if count==0:
#                 plot=ax.plot(x_track_dict[k][i], y_track_dict[k][i],
#                     color=colors[k],alpha=0.5, label=times[k])
#             else:
#                 plot=ax.plot(x_track_dict[k][i], y_track_dict[k][i],
#                     color=colors[k],alpha=(0.5/(k+1)+0.4))
#             count+=1
    
#     #for i in x_long:
#     #    ax.plot(x_long[i], y_long[i], color='mediumorchid', alpha=0.5, 
#     #            label=">20 min")
#     #for i in x_short:
#     #    ax.plot(x_short[i], y_short[i], color='mediumvioletred', alpha=0.5,
#     #            label="<20 min")
#     ax.set_xlabel("x")
#     ax.legend()
#     ax.set_ylabel("y")
#     ax.set_ylim(-1.75,1.75)
#     ax.set_xlim(-1.75, 1.75)
#     ax.set_aspect(1./ax.get_data_ratio())
#     ax.set_title("Tracks on {} nights in 201{}".format(nights, year))
    
    
    
        