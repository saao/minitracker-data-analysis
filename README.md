# Data Analysis of SALT data for MiniTracker Project

Data analysis was done on the tracking behaviour of SALT with some minor modelling of tracks available to MiniTrackers of different configurations.
This was done during Covid lockdown in 2020 by [Freya Bovim](freyabovim@gmail.com) at request of Retha Pretorius. 

The results of this analysis were presented to the SALT board in May 2020. Please contact [Retha Pretorius](retha@saao.ac.za) for the presentable material.

Below, under Data Handling you can find a short explation of how the results were achieved.

In /scripts you can find some examples of the Python code used, and in /figs you can find some examples of the figures that were generated.
Please forgive the chaotic nature of this code; I didn't think anyone would ever bother to look at it.

## Data Handling:

- Data was downloaded from the [Web ELS site](http://webels.salt.saao.ac.za/els), for years that were available (mostly 2019 & 2014 due to some database problems).
	- The following data was selected:
		- TCS/status/: timestamp + tcs_mode + tcs_mode_desc 
			- This gives the status of the TCS (off/ready/tracking etc) at each timestamp, for the period selected 
		- TC/trajectory_status/: timestamp + x + y
			- This gives the tracker's coordinates at each timestamp for the period selected
	- NB: Check Settings> Limit Data Points> on the web-interface to ensure you download the entire dataset for the period selected
- Timestamps were converted to MJD
- The hours of day time (between winter sunrise and sunset times) were removed from the data
- Time periods where the tracker status wasn't track or guide (indicated by mode != 4 or 5 in the dataset) were removed from the data and start stop times were thereby calculated by changes in status
- The data sets of the tracker's coordinates were then filtered by matching the timestamps guide/track start-stop times
- Removed tracks<10 min
- Drew mirror as polygon using Python Shapely, ignoring gaps between mirrors
- Drew annulus for SALT and for MTs, calculated overlap
- After that it was just generating different ways of looking at the data, on Retha�s request
- Later added data sets of azimuth (TCS/telescope info/structure az angle and dome az angle) and similarly filtered and combined by timestamps

## Some Notes:

### Code:
- The code uses some iffy IDE + Python console combinations, commenting out heavy duty parts of code to not run when not necessary, etc due to computing constraints
- Made lookup tables to look at effective mirror for SALT, MTs at each tracker xy, and interpolate for similar reasons
- Used Python pickle to store variables between runs (these are the .p files that you can see in the Git history)
- Used some very weird dicts of dicts to seperate tracks out into different lengths etc

### Behaviour
- Had some trouble with unrealistically joined tracks zigzagging, going to far edges of xy
- But often, after a lot of effort to remove these outliers, the qualitative look of the data remained the same 
- We didn't factor in vignetting by the dome at extreme MT coordinates
